#ifndef MSLC_H
#define MSLC_H

#if defined (__cplusplus)
extern "C" {
#endif
  result_t MSSP_mslc_weighted(int beta, SPINT *fd, SPINT *hd, SPINT *ln,
			      SPINT ns, int64_t *s, node_t *nd, SPINT *ds, heap_t *hq);
  result_t MSSP_mslc(int beta, SPINT *fd, SPINT *hd, SPINT *ln,
		     SPINT ns, int64_t *s, node_t *nd, SPINT *ds, heap_t *hq);

#if defined (__cplusplus)
}
#endif

#endif /* MSLC_H */

