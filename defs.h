#ifndef DEFS_H
#define DEFS_H

#define  U8_t unsigned char
#define I32_t   signed int
#define U32_t unsigned int
#define I64_t   signed long long int
#define U64_t unsigned long long int
#define U32_MAX ((U32_t)(~0))
#define I32_MAX ((I32_t)(U32_MAX >> 1))
#define U64_MAX ((U64_t)(~0))
#define I64_MAX ((I64_t)(U64_MAX >> 1))
#define I64_MIN ((I64_t)(-I64_MAX - 1LL))

#ifndef PROFILED
#  define PROFILED(t, X) do { \
    double tt=get_msecs();    \
    X;			      \
    t=get_msecs()-tt;	      \
  } while (0)
#endif

#ifndef LINEMAX
#define LINEMAX 256
#endif
#ifndef MAX_NODES
#define MAX_NODES 256
#endif
#ifndef MAX_THREADS
#define MAX_THREADS 4096
#endif
#ifdef NETAL_ILP64
#define SPINT_MAX I64_MAX
#define SPINT     I64_t
#else
#define SPINT_MAX I32_MAX
#define SPINT     I32_t
#endif

#include <stdint.h>
#include <inttypes.h>

#include "version.h"
#include "graph.h"
#include "cent_kernel.h"
#include "dist_kernel.h"

struct profile_t {
  I64_t iterations;
  struct cent_result_t total;
  struct cent_result_t *results;
};

#endif /* DEFS_H */
