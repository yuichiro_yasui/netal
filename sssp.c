#include <stdio.h>
#include <stdlib.h>

#include "defs.h"
#include "utls.h"
#include "graph.h"
#include "heap.h"
#include "sssp.h"

#ifdef K_CENT
#define FUNCNAME0 SP_kSSSP_dijkstra
#define FUNCNAME1 MP_kSSSP_dijkstra
#define FUNCNAME2 MP_kBFS
#else
#define FUNCNAME0 SP_SSSP_dijkstra
#define FUNCNAME1 MP_SSSP_dijkstra
#define FUNCNAME2 MP_BFS
#endif


/* ------------------------------------------------------------
 * singlepath single-source shortest path SP_SSSP
 * ------------------------------------------------------------ */
result_t FUNCNAME0(SPINT *fd, SPINT *hd, SPINT *ln, SPINT s, SPINT t,
                   node_t *nd, heap_t *hq, SPINT *succ
#ifdef K_CENT
                   , SPINT k, SPINT *hop
#endif
                   ) {
  SPINT j, v, w, l, hqsz = 0;
  SPINT ites = 0, scanned = 0;
  SPINT CC = 0, GC = 0;
  
  nd[s].key = 0;
#ifdef K_CENT
  hop[s] = 0;
#endif
  v = s;
  
  do {
#ifdef K_CENT
    if (hop[v] <= k) {
#endif
      scanned += fd[v+1] - fd[v];
      CC += nd[v].key;
      GC  = nd[v].key;
      
      for (j = fd[v]; j < fd[v+1]; ++j) {
        w = hd[j];
        l = ln[j];
      
        /* w found for the first time? */
        if (nd[w].key < 0) {
          nd[w].key = nd[v].key + l;
          succ[w] = v;
#ifdef K_CENT
          hop[w] = hop[v] + 1;
#endif
          nd[w].offset = hqsz++;
          heapify(hq, nd, w);
        } else if (nd[w].key > nd[v].key + l) {
          nd[w].key = nd[v].key + l;
          succ[w] = v;
#ifdef K_CENT
          hop[w] = hop[v] + 1;
#endif
          heapify(hq, nd, w);
        }
      }
#ifdef K_CENT
    }
#endif
    
    /* extractmin */
    if (--hqsz < HQROOT) {
      v = -1;
      break;
    } else {
      v = hq[HQROOT].id;
      nd[ hq[HQROOT].id ].offset = -1;
      extractmin(hq, nd, hqsz);
    }
  } while (v != t);
  
  result_t r;
  r.trav_nodes = ites;
  r.trav_edges = scanned;
  r.CC = (double)CC;
  r.GC = (double)GC;
  return r;
}


/* ------------------------------------------------------------
 * multipath single-source shortest path MP_SSSP
 * ------------------------------------------------------------ */
/* #ifdef K_CENT */
/* int64_t number_of_nodes1 = 0, number_of_edges1 = 0; */
/* /\* #define __ASSERT(x) assert( x < number_of_edges1 ); *\/ */
/* #define __ASSERT(x) \ */
/*   if ( x >= number_of_edges1 ) printf("x: %lld >= %lld, %d\n", x, number_of_edges1, __LINE__); */
/* #else */
/* int64_t number_of_nodes2 = 0, number_of_edges2 = 0; */
/* /\* #define __ASSERT(x) assert( x < number_of_edges2 ); *\/ */
/* #define __ASSERT(x) \ */
/*   if ( x >= number_of_edges2 ) printf("x: %lld >= %lld, %d\n", x, number_of_edges2, __LINE__); */
/* #endif */


result_t FUNCNAME1(SPINT *fd, SPINT *hd, SPINT *ln, SPINT s,
                   node_t *nd, heap_t *hq, succ_t *list, SPINT *count,
                   SPINT *queue, double *sigma
#ifdef K_CENT
                   , SPINT k, SPINT *hop
#endif
                   ) {
  SPINT j, v, w, l, hqsz = 0;
  SPINT ites = 0, scanned = 0;
  SPINT CC = 0, GC = 0;
  
  sigma[s] = 1.0;
  nd[s].key = 0;
#ifdef K_CENT
  hop[s] = 0;
#endif
  v = s;
  
  do {
#ifdef K_CENT
    if (hop[v] <= k) {
#endif
      queue[ites++] = v;	/* enqueue */
      scanned += fd[v+1] - fd[v];
      CC += nd[v].key;
      GC  = nd[v].key;
      
      for (j = fd[v]; j < fd[v+1]; ++j) {
        w = hd[j];
        l = ln[j];
      
        if (nd[w].key < 0) {    /* w found for the first time? */
          nd[w].key = nd[v].key + l;
#ifdef K_CENT
          hop[w] = hop[v] + 1;
#endif
          nd[w].offset = hqsz++;
          heapify(hq, nd, w);
          sigma[w] = sigma[v];	/* reset # of shortest-path */
          count[v] = fd[v];
          /* __ASSERT( count[v] ); */
          list[ count[v] ].w = w;  /* clear list */
          list[ count[v] ].v_w = j;
          ++count[v];
        } else if (nd[w].key > nd[v].key + l) {
          nd[w].key = nd[v].key + l;
#ifdef K_CENT
          hop[w] = hop[v] + 1;
#endif
          heapify(hq, nd, w);
          sigma[w] = sigma[v];	/* reset # of shortest-path */
          count[v] = fd[v];
          /* __ASSERT( count[v] ); */
          list[ count[v] ].w = w;  /* clear list */
          list[ count[v] ].v_w = j;
          ++count[v];
        } else if (nd[w].key == nd[v].key + l) { /* shortest path to w via v */
          sigma[w] += sigma[v];
          /* __ASSERT( count[v] ); */
          list[ count[v] ].w = w;
          list[ count[v] ].v_w = j;
          ++count[v];
        }
      }
#ifdef K_CENT
    }
#endif
    
    /* extractmin */
    if (--hqsz < HQROOT) {
      v = -1;
    } else {
      v = hq[HQROOT].id;
      nd[ hq[HQROOT].id ].offset = -1;
      extractmin(hq, nd, hqsz);
    }
  } while (v >= 0);
  
  result_t r;
  r.trav_nodes = ites;
  r.trav_edges = scanned;
  r.CC = (double)CC;
  r.GC = (double)GC;
  return r;
}


/* ------------------------------------------------------------
 * multipath single-source shortest path MP_BFS
 * ------------------------------------------------------------ */
result_t FUNCNAME2(SPINT *fd, SPINT *hd, SPINT s, 
                   SPINT *ds, succ_t *list, SPINT *count,
                   SPINT *queue, double *sigma
#ifdef K_CENT
                   , SPINT k
#endif
                   ) {
  SPINT j, v, w, start, end;
  SPINT scanned = 0;
  SPINT CC = 0, GC = 0;

  sigma[s] = 1.0;
  ds[s] = 0;
  start = end = 0; /* init-queue */
  queue[end++] = s; /* enqueue */

  while (end - start) {
    v = queue[start++];	/* dequeue */
    CC += ds[v];
    GC  = ds[v];
    scanned += fd[v+1] - fd[v];
    count[v] = fd[v];
    /* printf("v = %lld : ", v+1); */
    for (j = fd[v]; j < fd[v+1]; ++j) {
      w = hd[j];
      
      /* w found for the first time? */
      if (ds[w] < 0) {
	ds[w] = ds[v] + 1;
#ifdef K_CENT
        if (ds[w] <= k) {
#endif
          queue[end++] = w;
#ifdef K_CENT
        }
#endif
	sigma[w] = sigma[v];
        list[ count[v] ].w = w;
        list[ count[v] ].v_w = j;
        ++count[v];
        /* printf("%lld, ", w+1); */
      } else if (ds[w] == ds[v] + 1) { /* shortest path to w via v */
	sigma[w] += sigma[v];
        list[ count[v] ].w = w;
        list[ count[v] ].v_w = j;
        ++count[v];
        /* printf("%lld, ", w+1); */
      }
    }
    /* printf("\n"); */
    /* printf("s = %ld, sigma[%ld] = %ld\n", s+1, v+1, count[v]-fd[v]); */
  }
  /* printf("\n"); */
  
  result_t r;
  r.trav_nodes = end;
  r.trav_edges = scanned;
  r.CC = (double)CC;
  r.GC = (double)GC;
  return r;
}


/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 */
