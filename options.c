#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "ulibc.h"
#include "version.h"
#include "utls.h"
#include "defs.h"
#include "options.h"
#include "graph.h"

#define MAX_USAGE 256
#define MAX_OPTS   16

const char *desc =
  "----------------------------------------------------------------------\n"
  "NETAL (NETwork Analysis Library)\n"
  "Version: " NETAL_VERSION "\n"
  "Copyright: " NETAL_COPYRIGHT "\n"
  "----------------------------------------------------------------------\n";

const char *quickusage =
  "Usage: ./netal -i DIMACS.gr -z KERNEL.name KERNEL.opt -o RESULT.sp [-v {0,1,2}]";

const char *exusage = "";
#if 0
const char *exusage =
  "   ex) ./netal -i USA-road-d.NY.gr -z cent uB 1.00 -v 0 -o NY_exact_uw_BC_eBC.sp";
#endif

/* 2nd level */
struct usage_t graph_type_usages[] = {
  { "dimacs", "", "9th DIMACS format graph file (.gr)", NULL },
  { "snap",   "", "SNAP graph file",                    NULL },
  { "metis",  "", "METIS graph file",                   NULL },
#if 0
  { "binary", "", "NETAL binary graph file",            NULL },
#endif
  { NULL, NULL, NULL, NULL }
};

/* 3rd level */
struct usage_t cent_usages[] = {
  {  "m", NULL,    "for max-CC", NULL },
  {  "s", NULL,      "sequence", NULL },
  {  "u", NULL,    "Unweighted", NULL },
  {  "w", NULL,      "Weighted", NULL },
  {  "B", NULL,   "Betweenness", NULL },
  {  "C", NULL,     "Closeness", NULL },
  {  "D", NULL,        "Degree", NULL },
  {  "E", NULL,    "Efficiency", NULL },
  {  "G", NULL,         "Graph", NULL },
  {  "S", NULL,        "Stress", NULL },
  {  "T", NULL,  "Straightness", NULL },
  { NULL, NULL, NULL, NULL },
};
struct usage_t comp_usages[] = {
  {  "c", NULL,         "color", NULL },
  {  "m", NULL,        "max-CC", NULL },
  {  "s", NULL,     "sub-graph", NULL },
  { NULL, NULL, NULL, NULL },
};
struct usage_t mssp_usages[] = {
  {  "D", NULL, "distance-based centrality", NULL },
  {  "w", NULL,      "Weighted", NULL },
  {  "u", NULL,    "Unweighted", NULL },
#if 0
  {  "m", NULL,    "for max-CC", NULL },
  {  "a", NULL,          "APSP", NULL },
  {  "S", NULL,    "singlepath", NULL },
  {  "M", NULL,     "multipath", NULL },
#endif
  { NULL, NULL,            NULL, NULL },
};

/* 2rd level */
struct usage_t kernel_usages[] = {
  { "cent", "identifier  accuracy", "# muliple centrality metrics", cent_usages },
  { "mssp", "identifier  accuracy", "# shortest-paths",              mssp_usages },
#if 0
  { "comp", "identifier          ", "# connected components",       comp_usages },
#endif
  { NULL, NULL, NULL, NULL }
};

/* top level */
struct usage_t usages[] = {
  { "-i", "FILE",   "graph filename",        NULL },
  { "-t", "TYPE",   "graph type",            graph_type_usages }, 
  { "-q", "FILE",   "query filename",        NULL },
  { "-o", "FILE",   "result filename",       NULL },
  { "-z", "KERNEL", "kernel options",        kernel_usages },
  { "-v", "LEVEL",  "verbose level {0,1,2}", NULL },
  { "-h", "",       "\thelp mode",           NULL },
  { NULL, NULL, NULL, NULL },
};

/* top level */
struct usage_t env_usages[] = {
  { "OMP_NUM_THREADS=NUM", "", "set number of threads to 'NUM'.",              NULL },
  { "DIMACS_CO=FILE",      "", "dimacs coordinates file",                      NULL },
  { "MSLC_BETA=NUM",       "", "set number of sources to 'NUM'.",              NULL },
  { "TIMELIMIT=NUM",       "", "time limit (seconds)",                         NULL },
  { "UNDIRECTED=NUM",      "", "0: directed (default), 1: undirected",         NULL },
#if 0
  { "DUMPEDGES=FILE",      "", "dump edge file",                               NULL },
  { "DUMPETYPE=FILE",      "", "dump edge type: dimacs (default), binary",     NULL },
  { "DUMPGRAPHCT=FILE",    "", "dump BC as GraphCT format",                    NULL },
  { "DUMPSRCS=FILE",       "", "dump partitioned nodes",                       NULL },
  { "HOPBOUND=NUM",        "", "hops-bound (solve 0..NUM)",                    NULL },
  { "FALL_TOLERANCE=NUM",  "", "0: off (default), 1: fall tolerance",          NULL },
  { "DUMPUNIQTABLE=FILE",  "", "0: off (default), 1: dump uniq node table",    NULL },
#endif
  { NULL, NULL, NULL, NULL }
};


int is_known(struct usage_t *usages, const char *s);
int symbol_to_flag(struct usage_t *usages, char *s);
char *flag_to_symbol(struct usage_t *usages, int flag);
static int sprintf_usagemsg(char *usagemsg, struct usage_t *usages);

struct netal_option_t get_netal_options(int argc, char **argv) {
  int i, j;
  struct netal_option_t option;
  memset(&option, 0x00, sizeof(struct netal_option_t));
  option.verbose = 0;
  
  /* construct usagemsg */
  char usagemsg[1 << 16]="";
  sprintf(usagemsg, "%s::::: options :::::\n", usagemsg);
  sprintf_usagemsg(usagemsg, usages);
  sprintf(usagemsg, "%s::::: envs :::::\n", usagemsg);
  sprintf_usagemsg(usagemsg, env_usages);
  
  /* ------------------------------------------------------------ *
   * parsing options without getopt/getopt_long routines
   * ------------------------------------------------------------ */
  for (i = 1; i < argc; ++i) {
    /* printf("%d:%s\n", i, argv[i]); */
    
    /* ========== unknown option ========== */
    if ( !is_known(usages, argv[i]) ) {
      fprintf(stdout, "[error] unknown option '%s'\n", argv[i]);
      exit(1);
    }
    
    /* ========== graph_file ========== */
    if ( !strcmp(argv[i], "-i") ) {
      if ( !(++i < argc) || is_known(usages, argv[i]) ) {
        fprintf(stdout, "[error] not specified graph file\n");
        fprintf(stdout, "\t-i GRAPHFILE\n");
        exit(1);
      }
      option.graph_file = strdup(argv[i]);
      /* printf("%s\n", option.graph_file); */
    }
    
    /* ========== graph_type ========== */
    if ( !strcmp(argv[i], "-t") ) {
      if ( !(++i < argc) || is_known(usages, argv[i]) ) {
        fprintf(stdout, "[error] not specified graph type\n");
        fprintf(stdout, "-t {'dimacs', "
#if 0
		"'binary'"
#endif
		", 'snap', 'metis'}\n");
        exit(1);
      }
      
      if ( !strcmp(argv[i], "dimacs") ) {
	option.graph_type = GRAPH_DIMACS;
	option.graph_desc = strdup("dimacs");
	/* } else if ( !strcmp(argv[i], "binary") ) { */
	/* 	option.graph_type = GRAPH_BINARY; */
	/* 	option.graph_desc = strdup("binary"); */
      } else if ( !strcmp(argv[i], "snap") ) {
	option.graph_type = GRAPH_SNAP;
	option.graph_desc = strdup("snap");
      } else if ( !strcmp(argv[i], "metis") ) {
	option.graph_type = GRAPH_METIS;
	option.graph_desc = strdup("metis");
      } else {
        fprintf(stdout, "[error] unknown graph type '%s'\n", argv[i]);
        fprintf(stdout, "-t {'dimacs', "
#if 0
		"'binary'"
#endif
		", 'snap', 'metis'}\n");
        exit(1);
      }
      /*       printf("%d %s\n", option.graph_type, option.graph_desc); */
    }

    /* ========== dump_file ========== */
    if ( !strcmp(argv[i], "-o") ) {
      if ( !(++i < argc) || is_known(usages, argv[i]) ) {
        fprintf(stdout, "[error] not specified dump file\n");
        fprintf(stdout, "-o DUMPFILE\n");
        exit(1);
      }
      option.dump_file = strdup(argv[i]);
    }

    /* ========== query_file ========== */
    if ( !strcmp(argv[i], "-q") ) {
      if ( !(++i < argc) || is_known(usages, argv[i]) ) {
        fprintf(stdout, "[error] not specified query file\n");
        fprintf(stdout, "-o QUERYFILE\n");
        exit(1);
      }
      option.query_file = strdup(argv[i]);
    }

    /* ========== kernel ========== */
    if ( !strcmp(argv[i], "-z") ) {
      if ( !(++i < argc) || !is_known(kernel_usages, argv[i]) ) {
	if (i == argc) {
	  fprintf(stdout, "[error] not specified kernel\n");
	} else {
	  fprintf(stdout, "[error] not unknown analysis kernel '%s'\n", argv[i]);
	}
	char *opt = "-z";
	struct usage_t *us = kernel_usages;
	for (j = 0; us[j].name; ++j) {
	  printf("\t%s  %s  %s\t\t[%s]\n", 
		 opt, us[j].name, us[j].args, us[j].detail);
	}
        exit(1);
      }
      
      int f = 0;
      
      /* ---------- centrality ---------- */
      if ( !f && !strcmp(argv[i], "cent") ) {
	f = 1;
        option.kernel_id = KERNEL_CENT;
        if ( i+2 < argc ) {
	  option.kernel_flag = symbol_to_flag(cent_usages, argv[++i]);
	  if (option.kernel_flag == -1) {
	    fprintf(stdout, "[error] found unknown kernel options '%s'\n\n", argv[i]);
	    exit(1);
	  }
          option.accuracy  = atof(argv[++i]);
          option.sequence  = option.kernel_flag & (1<<1);
          option.weighted  = option.kernel_flag & (1<<3);
          option.hop_bound = getenvi("HOPBOUND", 0);
	  assert(0.00 <= option.accuracy && option.accuracy <= 1.00);
	  sprintf(option.kernel_name, "centrality (%s, acc=%f%%, %s%s)",
		  flag_to_symbol(cent_usages, option.kernel_flag),
		  option.accuracy*100.0,
		  option.weighted ? "weighted" : "unweighted",
		  option.sequence ? "" : ", random sampling");
	  /* printf("kernel is %d %s\n", option.kernel_flag, option.kernel_name); */
        } else {
	  fprintf(stdout, "[error] not specified centrality kernel options\n");
	  char *opt = "-z";
	  struct usage_t *us = cent_usages;
          fprintf(stdout, "%s  %s  %s\t\t%s\n",
		  opt, kernel_usages[0].name,
		  kernel_usages[0].args, kernel_usages[0].detail);
	  for (j = 0; us[j].name; ++j) {
	    printf("\t'%s': %s\n", us[j].name, us[j].detail);
	  }
          exit(1);
        }
      }
      
      /* ---------- connected-components ---------- */
#if 0
      if ( !f && !strcmp(argv[i], "comp") ) {
      	f = 1;
	option.kernel_id = KERNEL_COMP;
	if ( i+1 < argc ) {
      	  option.kernel_flag = symbol_to_flag(comp_usages, argv[++i]);
      	  if (option.kernel_flag == -1) {
      	    fprintf(stdout, "[error] found unknown kernel options '%s'\n\n", argv[i]);
      	    exit(1);
      	  }
      	  sprintf(option.kernel_name, "connected-components %s",
      		  flag_to_symbol(cent_usages, option.kernel_flag));
      	  /* printf("kernel is %d %s\n", option.kernel_flag, option.kernel_name); */
	} else {
      	  fprintf(stdout,
      		  "[error] not specified connected-components kernel options\n");
      	  char *opt = "-z";
      	  struct usage_t *us = comp_usages;
	  fprintf(stdout, "%s  %s  %s\t\t%s\n",
      		  opt, kernel_usages[1].name,
      		  kernel_usages[1].args, kernel_usages[1].detail);
      	  for (j = 0; us[j].name; ++j) {
      	    printf("\t'%s': %s\n", us[j].name, us[j].detail);
      	  }
	  exit(1);
	}
      }
#endif
      
      /* ---------- shortest-paths ---------- */
      if ( !f && !strcmp(argv[i], "mssp") ) {
	f = 1;
        option.kernel_id = KERNEL_MSSP;
        if ( i+2 < argc ) {
	  option.kernel_flag = symbol_to_flag(mssp_usages, argv[++i]);
	  if (option.kernel_flag == -1) {
	    fprintf(stdout, "[error] found unknown kernel options '%s'\n\n", argv[i]);
	    exit(1);
	  }
          option.accuracy = atof(argv[++i]);
          option.weighted = (option.kernel_flag & (1<<1));
	  /* printf("option.weighted: %d\n", option.weighted); */
	  /* exit(1); */
	  assert(0.00 <= option.accuracy && option.accuracy <= 1.00);
	  sprintf(option.kernel_name, "shortestpaths (%s, acc=%f%%, %s)",
		  flag_to_symbol(cent_usages, option.kernel_flag),
		  option.accuracy*100.0, option.weighted ? "weighted" : "unweighted");
        } else {
          fprintf(stdout, "[error] not specified shortest-paths kernel options\n");
	  char *opt = "-z";
	  struct usage_t *us = mssp_usages;
          fprintf(stdout, "%s  %s  %s\t\t%s\n",
		  opt, kernel_usages[1].name,
		  kernel_usages[1].args, kernel_usages[1].detail);
	  for (j = 0; us[j].name; ++j) {
	    printf("\t'%s': %s\n", us[j].name, us[j].detail);
	  }
          exit(1);
        }
      }
      /* end of loop */
    }
    
    /* ========== verbose ========== */
    if ( !strcmp(argv[i], "-v") ) {
      if ( !(++i < argc) ) {
        fprintf(stdout, "[error] not specified verbose level\n");
        exit(1);
      }
      option.verbose = atoi(argv[i]);
    }
    
    /* ========== help ========== */
    if ( !strcmp(argv[i], "-h") ) {
      fprintf(stdout, "%s\n%s\n\n%s\n\n%s", desc, quickusage, exusage, usagemsg);
      exit(1);
    }
  }
  
  option.hop_bound = getenvi("HOP_BOUND",  0);
  option.mslc_beta = getenvi("MSLC_BETA", 32);
  
  
  /* default graph type is DIMACS */
  if ( !option.graph_desc ) {
    option.graph_type = GRAPH_DIMACS;
    option.graph_desc = strdup("dimacs");
  }
  
  /* exit(1); */
  
  return option;
}


void finalize_netal_options(struct netal_option_t option) {
  free(option.graph_file);
  free(option.graph_desc);
  free(option.dump_file);
}




static int sprintf_usagemsg(char *usagemsg, struct usage_t *usages) {
  int i, j, k, nw=0;
  
  if ( !usagemsg || !usages ) {
    return nw;
  }
  
  /* top level */
  for (i = 0; usages[i].name; ++i) {
    char *opt = usages[i].name;
    nw += sprintf(usagemsg, "%s  %s  %s\t\t%s\n",
		  usagemsg, usages[i].name, usages[i].args, usages[i].detail);
    if (!usages[i].sub) continue;
    
    /* 2nd level */
    struct usage_t *sub_usage = usages[i].sub;
    for (j = 0; sub_usage[j].name; ++j) {
      nw += sprintf(usagemsg, "%s\t%s  %s  %s\t\t%s\n", usagemsg,
		    opt, sub_usage[j].name, sub_usage[j].args, sub_usage[j].detail);
      if (!sub_usage[j].sub) continue;
      
      /* 3rd level */
      struct usage_t *sub_sub_usage = sub_usage[j].sub;
      nw += sprintf(usagemsg, "%s\t\t", usagemsg);
      for (k = 0; sub_sub_usage[k].name; ++k) {
      	nw += sprintf(usagemsg, "%s\t'%s' : %-10s", usagemsg,
		      sub_sub_usage[k].name, sub_sub_usage[k].detail);
      	if (!((k+1)%2)) sprintf(usagemsg, "%s\n\t\t", usagemsg);
      }
      nw += sprintf(usagemsg, "%s\n", usagemsg);
    }
    nw += sprintf(usagemsg, "%s\n", usagemsg);
  }
  nw += sprintf(usagemsg, "%s\n", usagemsg);
  
  return nw;
}


int symbol_to_flag(struct usage_t *usages, char *s) {
  int i, j;
  int flag = 0;
  /* printf("in: %s\n", s); */
  for (i = 0; s[i]; ++i) {
    for (j = 0; usages[j].name; ++j) {
      if ( s[i] == usages[j].name[0] ) {
	flag |= (1 << j);
	break;
	/* printf("%d:%c %d\n", i, s[i], (1 << j)); */
      }
    }
    if (!usages[j].name) return -1;
  }
  return flag;
}


char *flag_to_symbol(struct usage_t *usages, int flag) {
  int j, k=0;
  static char buf[128];
  for (j = 0; usages[j].name; ++j) {
    if (flag & (1 << j)) {
      buf[k++] = usages[j].name[0];
      /* printf("%c\n", get_symbol(usages, j)); */
    }
  }
  buf[k] = '\0';
  return buf;
}


int is_known(struct usage_t *usages, const char *s) {
  int i;
  for (i = 0; usages[i].name; ++i) {
    if ( !strcmp(s, usages[i].name) ) 
      return 1;
  }
  return 0;
}
