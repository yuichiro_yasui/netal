#ifndef DIST_H
#define DIST_H

typedef struct degdist degdist;

struct degdist {
  SPINT maxdeg;
  double avg, avg_sq, var, std_dev;
};

#if defined (__cplusplus)
extern "C" {
#endif
  degdist degree_distributions(SPINT n, SPINT *fwd);
#if defined (__cplusplus)
}
#endif

#endif /* DIST_H */
