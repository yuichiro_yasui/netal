#ifndef CENT_VARIANT_H
#define CENT_VARIANT_H

/* number of outgoing degrees */
#define DC_BASE(n) (1.0)
/* #define DC_BASE(n) (1.0,max_degree) */

/* shortest path lengths */
#define CC_BASE(n) (1.0/((n)*((n)-1)))
#define GC_BASE(n) (1.0/((n)-1))

/* shortest path lengths and coordinates */
#define EC_BASE(n) (1.0)
#define TC_BASE(n) (1.0/((n)-1))

/* number of shortest paths */
/* #define BC_BASE(n) (1.0/(((n)-1)*((n)-2))) */
/* #define SC_BASE(n) (1.0/(((n)-1)*((n)-2))) */
#define BC_BASE(n) (1.0)
#define SC_BASE(n) (1.0)

/* normalize coordinates */
#define COORD_NORMALIZE(n) (1.0/(n))

#if defined (__cplusplus)
extern "C" {
#endif
#if defined (__cplusplus)
}
#endif

#endif /* CENT_VARIANT_H */
