#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <stdint.h>
#include <assert.h>
#include <ulibc.h>

#include "defs.h"
#include "utls.h"
#include "graph.h"
#include "rand_sampling.h"


/* ------------------------------------------------------------ *
 * make roots
 * ------------------------------------------------------------ */
static int64_t *make_source_list(graph *G, int64_t *nv_addr);
static int64_t *load_source_list(const char *filename, graph *G, int64_t *nv_addr);
static void dump_source_list(const char *filename, int64_t nv, int64_t *roots);

int64_t *make_roots(int isseq, graph *G, int64_t *nv_addr) {
  assert(nv_addr);
  
  int64_t *roots = NULL, nv = 0;

  /* load or make */
  if ( getenv("LOADSRCS") ) {
    char *ss_file = getenv("LOADSRCS");
    printf("source sequence from source file '%s'\n", ss_file);
    roots = load_source_list(ss_file, G, &nv);
  } else {
    roots = make_source_list(G, &nv);
    if ( isseq ) {
      printf("sampling %" PRId64 " sources from %" PRId64 " nodes\n", nv, G->n);
    } else {
      randmized(nv, roots);
      printf("random sampling %" PRId64 " sources from %" PRId64 " nodes\n", nv, G->n);
    }
  }

  /* storing sampling sources */
  if ( getenv("DUMPSRCS") ) {
    const char *ss_file = getenv("DUMPSRCS");
    printf("output source file '%s'\n", ss_file);
    dump_source_list(ss_file, nv, roots);
  }

  *nv_addr = nv;
  
  return roots;
}


static int64_t *make_source_list(graph *G, int64_t *nv_addr) {
  /* found vertices */
  unsigned char *x = (unsigned char *)calloc(G->n, sizeof(unsigned char));
  for (int64_t v = 0; v < G->n; v++) {
    if ( G->forward[v+1] > G->forward[v] )
      x[v] = 1;
    for (int64_t j = G->forward[v]; j < G->forward[v+1]; ++j)
      x[ G->head[j] ] = 1;
  }

  /* count number of vertices */
  int64_t nv = 0;
  for (int64_t v = 0; v < G->n; ++v) {
    if ( x[v] ) ++nv;
  }
  printf("found %" PRId64 " vertices\n", nv);

  /* allocate and fill roots array */
  int64_t *roots = NULL;
  roots = (int64_t *)calloc(nv, sizeof(int64_t));
  assert( roots );

  for (int64_t v = 0, j = 0; v < G->n; ++v) {
    if ( x[v] )
      roots[j++] = v;
  }
  
  if (nv_addr)
    *nv_addr = nv;
  
  free(x);
  return roots;
}


static int64_t *load_source_list(const char *filename, graph *G, int64_t *nv_addr) {
  FILE *fp = NULL;
  if (filename) {
    fp = fopen(filename, "r");
  }
  if (!fp) {
    fprintf(stderr, "warning : can't open query file '%s'\n", filename);
    return NULL;
  }
  
  U8_t *check = (U8_t *)calloc(G->n, sizeof(U8_t));
  
  /* read */
  char line[LINEMAX+1];
  int64_t nv = 0, *roots = NULL;
  while ( fgets(line, LINEMAX, fp) ) {
    if (line[0] == 's') {
      const int64_t s = strtoll(&line[1], NULL, 10) - 1;
      if ( !check[s] ) {
	roots[nv++] = s;
	check[s] = 1;
      }
    } else if (line[0] == 'p') {
      char ide[8], pro[8], fmt[8];
      long long ns = 0;
      sscanf(&line[1], "%s %s %s %lld", ide, pro, fmt, &ns);
      assert( !strcmp(ide, "aux") );
      assert( !strcmp(pro, "sp" ) );
      assert( !strcmp(fmt, "ss" ) );
      roots = (int64_t *)xmalloc((G->n+ns) * sizeof(int64_t));
      printf("'%s' contains %lld vertices\n", filename, ns);
    }
  }
  fclose(fp);

  /* fill */
  for (int64_t j = 0; j < G->n; ++j) {
    if (check[j] == 0) {
      roots[nv++] = j;
      check[j] = 1;
    }
  }
  assert( nv == G->n );
  free(check);

  *nv_addr = nv;
  
  return roots;
}


static void dump_source_list(const char *filename, int64_t nv, int64_t *roots) {
  FILE *fp = NULL;
  if (filename) {
    fp = fopen(filename, "w");
  }
  if (fp) {
    fprintf(fp, "p aux sp ss %" PRId64 "\n", nv);
    fprintf(fp, "c\n");
    for (int64_t j = 0; j < nv; ++j) {
      fprintf(fp, "s %" PRId64 "\n", roots[j]+1);
    }
    fclose(fp);
  }
}




/* ------------------------------------------------------------
 * blocked_vertices
 * ------------------------------------------------------------ */
static void dump_blocked_vertices(const char *filename,
				  struct blocked_vertices_t *bv) {
  FILE *fp = NULL;
  if (filename) {
    fp = fopen(filename, "w");
  }
  if (fp) {
    fprintf(fp, "p aux sp ss %" PRId64 "\n", bv->nv);
    fprintf(fp, "c\n");
    fprintf(fp, "l block_index\n");
    fprintf(fp, "c\n");
    fprintf(fp, "c blocksize: %" PRId64 "\n", bv->block_size);
    fprintf(fp, "c nblocks: %" PRId64 "\n", bv->nblocks);
    fprintf(fp, "c\n");
    for (int64_t k = 0; k < bv->nblocks; ++k) {
      for (int64_t j = bv->start[k]; j < bv->start[k+1]; ++j) {
	fprintf(fp, "d %" PRId64 " %" PRId64 "\n", bv->sources[j]+1, k+1);
      }
    }
    fclose(fp);
  }
}


static int64_t breadth_first_local_vertices(graph *G, int64_t *fifo, int64_t *found,
					    int64_t curr, int64_t *stamp,
					    int64_t nv, int64_t *roots,
					    int64_t block_size, int64_t *sources) {
  /* BFS root */
  static int64_t static_counter = 0;
  int64_t r = -1;
  while (static_counter < nv) {
    r = roots[static_counter++];
    if ( found[r] == 0 ) break;
  }
  if ( static_counter == G->n ) return 0;

  if ( block_size == 1 ) {
    sources[0] = r;
    ++found[r];
    stamp[r] = curr;
    return 1;
  }

  /* BFS */
  int64_t first = 0, last = 0, nf = 0;
  fifo[last++] = r;
  stamp[r] = curr;
  while (last - first > 0) {
    const int64_t v = fifo[first++];
    if ( found[v] == 0 ) {
      ++found[v];
      assert( stamp[v] == curr );
      sources[nf++] = v;
      if (nf == block_size) break;
    }
    for (int64_t j = G->forward[v]; j < G->forward[v+1]; ++j) {
      const int64_t w = G->head[j];
      if ( stamp[w] != curr ) {
	stamp[w] = curr;
	fifo[last++] = w;
      }
    }
  }

  return nf;
}


static int64_t fill_local_vertices(int64_t n, int64_t *found,
				   int64_t curr, int64_t *stamp,
				   int64_t block_size, int64_t *sources) {
  int64_t nb = 0;
  for (int64_t i = 0; i < n; ++i) {
    if (found[i] == 0) {
      ++found[i];
      sources[nb++] = i;
      stamp[i] = curr;
      if (nb == block_size) break;
    }
  }
  return nb;
}


static int64_t get_nblocks(int64_t nv, int64_t block_size) {
  return nv/block_size + ((nv/block_size*block_size < nv) ? 1 : 0);
}


struct blocked_vertices_t make_blocked_vertices(graph *G, int64_t block_size) {
  /* initial list */
  int64_t nv = 0, *roots = NULL;
  roots = make_roots(0, G, &nv);

  struct blocked_vertices_t bv;
  bv.nv         = nv;
  bv.block_size = block_size;
  bv.nblocks    = get_nblocks(nv, bv.block_size);
  bv.start      = (int64_t *)xmalloc(sizeof(int64_t) * (bv.nblocks+2));
  bv.sources    = (int64_t *)xmalloc(sizeof(int64_t) * (G->n+1));
  printf("nv:         %" PRId64 "\n", bv.nv);
  printf("block_size: %" PRId64 "\n", bv.block_size);
  printf("nblocks:    %" PRId64 "\n", bv.nblocks);
  
  /* allocate temporary areas */
  int64_t *fifo, *stamp, *found;
  fifo  = (int64_t *)xmalloc( sizeof(int64_t) * (G->n) );
  stamp = (int64_t *)xmalloc( sizeof(int64_t) * (G->n) ); /* zero clear */
  found = (int64_t *)xmalloc( sizeof(int64_t) * (G->n) ); /* zero clear */
  
  /* partitioning */
  set_timer(0.01, 0.0);
  
  int64_t sum_found = 0;
  for (int64_t k = 0; k < bv.nblocks; ++k) {
    assert( sum_found == bv.block_size*k );
    /* printf("sum_found: %lld (%lld)\n", sum_found, bv.block_size*k); */
    int64_t nf1 = 0, nf2 = 0;
    nf1 = breadth_first_local_vertices(G, fifo, found, k+1, stamp, nv, roots,
				       bv.block_size, &bv.sources[sum_found]);
    if (nf1 != bv.block_size) {
      nf2 = fill_local_vertices(G->n, found, k+1, stamp,
				bv.block_size-nf1, &bv.sources[sum_found+nf1]);
    }
    bv.start[k] = sum_found;
    sum_found += (nf1 + nf2);
    
    if ( k == 0 || k == bv.nblocks-1 || interval() ) {
      printf("[%" PRId64 "/%" PRId64 "] |S|: %" PRId64 ", found: %" PRId64 ", "
      	     "elapsed: %7.2f ms. (%5.1f %%)\n",
      	     k+1, bv.nblocks, bv.start[k], sum_found,
	     toc(), 100.0*(k+1)/bv.nblocks);
    }
  }
  bv.start[bv.nblocks] = sum_found;

  for (int64_t i = 0; i < G->n; ++i) {
    if (found[i] != 1) {
      printf("i: %" PRId64 "\n", i+1);
    }
  }
  
  /* deallocate temporary areas */
  free(roots);
  free(fifo);
  free(stamp);
  free(found);
  
  if ( getenv("DUMPBLOCKEDROOTS") ) {
    const char *filename = getenv("DUMPBLOCKEDROOTS");
    printf("output blocked_roots file '%s'\n", filename);
    dump_blocked_vertices(filename, &bv);
  }
  return bv;
}
