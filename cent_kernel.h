#ifndef CENTRALITY_H
#define CENTRALITY_H

struct cent_param_t {
  int64_t num_srcs;
  int64_t startid;
  int weighted;
  int64_t hop_bound;
  int mslc_beta;
  int verbose;
  int sequence;
  double time_limit;
};

struct cent_metric_t {
  int64_t n;
  int64_t m;
  int64_t diam_hops;
  int64_t diam_id;
  double *DC;			/* [Degr] Degree centrality */
  double *CC;			/* [Dist] Closeness centrality */
  double *GC;			/* [Dist] Graph centrality */
  double *EC;			/* [Dist] Efficient centrality */
  double *TC;			/* [Dist] Straightness centrality */
  double *BC;			/* [Path] Betweenness centrality */
  double *eBC;			/* [Path] edge-Betweenness centrality */
  double *SC;			/* [Path] Stress centrality */
  double *eSC;			/* [Path] edge-Stress centrality */
};

struct cent_result_t {
  int64_t s;
  int64_t trav_nodes;
  int64_t trav_edges;
  int64_t max_hops;
  double sssp_ms;
  double update_ms;
};

/* ------------------------------------------------------------
 * Betweenness centrality
 *   - Freeman, L. C:
 *     A set of measures of centrality based on betweenness.
 *     Sociometry, 40:35-41 (1977).
 *   - Anthonisse, J. M.:
 *     The rush in a directed graph. Tech. Rep. BN 9/71,
 *     Stichting Mathematisch Centrum, 2e Boerhaavestraat 49 Amsterdam (1971).
 *
 * Closeness centrality
 *   - Sabidussi, G:
 *      The centrality index of a graph. Psychometrika, 31:581-603 (1966).
 * 
 * Graph centrality
 *   - Hage, P. and Harary, F. 
 *     Eccentricity and centrality in networks. Social Networks, 17:57-63 (1995).
 * 
 * Efficiency / Straightness
 *  
 * @article{PhysRevLett.87.198701,
 *   title = {Efficient Behavior of Small-World Networks},
 *   author = {Latora, Vito and Marchiori, Massimo},
 *   journal = {Phys. Rev. Lett.},
 *   volume = {87},
 *   issue = {19},
 *   pages = {198701},
 *   numpages = {4},
 *   year = {2001},
 *   month = {Oct},
 *   publisher = {American Physical Society},
 *   doi = {10.1103/PhysRevLett.87.198701},
 *   url = {http://link.aps.org/doi/10.1103/PhysRevLett.87.198701}
 * }
 * ------------------------------------------------------------ */

#if defined (__cplusplus)
extern "C" {
#endif
  struct profile_t compute_cent_kernel(graph *G,
				       struct cent_param_t *params,
				       struct cent_metric_t *metrics);
  void dump_cent_metrics(char *filename, char *desc, char *kernel,
			 graph *G,
			 struct cent_param_t *params,
			 struct cent_metric_t *metrics);
#if defined (__cplusplus)
}
#endif

#endif /* CENTRALITY_H */
