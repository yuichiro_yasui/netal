# NETAL


Overview
--------

__NETAL__ ( __NET__ work  __A__ nalysis __L__ ibrary ) <https://bitbucket.org/yuichiro_yasui/netal> is a software package for computing shortest-paths and centrality metrics based on them. has the  following features;

- Supports all-pairs shortest paths problem and many centrality metrics,
- Betweenness, edge-Betweenness, Closeness, Degree, Graph, Stress, and edge-Stress.
- NUMA-aware implementation of task-parallel computation using `ULIBC`, which is a library for detecting a processor topology and binding threads and memory on a NUMA system.

Reference to this work.

```
@article{Yasui_2010,
    author="Yasui, Yuichiro and Fujisawa, Katsuki and Goto, Kazushige and Kamiyama, Naoyuki and Takamatsu, Mizuyo",
    title="NETAL: High-performance implementation of network analysis library considering computer memory hierarchy",
    journal="Journal of the Operations Research Society of Japan",
    ISSN="04534514",
    publisher="The Operations Research Society of Japan",
    year="2011",
    month="dec",
    volume="54",
    number="4",
    pages="259-280",
    URL="http://www.orsj.or.jp/~archive/pdf/e_mag/Vol.54_04_259.pdf",
}
```








Build and Installation
----------------------

This section describes how to build and install the NETAL. We have done
build and installation tests on Linux platforms (RHEL, CentOS, Fedora, Debian,
Ubuntu, SUSE).

### Prerequisites
-   C compiler (GCC, Intel C Compiler, Oracle Solaris Studio)
-   `ULIBC`: <https://bitbucket.org/yuichiro_yasui/ulibc>

### How to Build
To build the NETAL, type the following.

```
$ sh
$ git clone https://bitbucket.org/yuichiro_yasui/ulibc.git
$ ( cd ulibc && make && make install )
$ git clone https://bitbucket.org/yuichiro_yasui/netal.git
$ cd netal && make
```


### Test Run
##### 1. Solving betweenness centrality of “`sample.gr`” on an unweighted directed graph

To get betweenness centrality of “`data/sample.gr`”, type the following.

```
% ./netal -i data/sample.gr -t dimacs -z cent uB 1.00 -o out
```

- The input graph "`-i data/sample.gr`" is compatible with the 9th-DIMACS format "`-t dimacs`".
- The solving kernel `-z cent uB 1.00` is an exact (`1.00`) vertex and edge betweenness metrics (`B`) based on unweighted shortest paths (`u`).
- The output file `-o out` is compatible with the NETAL format. Please see section for details of output file format.

NETAL outputs a following execution log and an output file.

```
% ./netal -i data/sample.gr -t dimacs -z cent uB 1.00 -o out
graph file     is data/sample.gr dimacs(0)
query file     is not specified
dump file      is out
kernel         is centrality (uB, acc=100.000000%, unweighted, random sampling)

parsing edge file ...
[c] c Shortest Paths : MSLC sample
[c] c
[p] p sp 9 12
[c] c This graph contains 9 nodes and 12 arcs
[c] c
	sample.gr (n=9, m=12)	##                             [  8%]
	sample.gr (n=9, m=12)	############################## [100%]
done. (0.08 msec.)
constructing graph file ...
done. (0.01 msec.)
graph          is n=9, m=12, edgefactor=m/n=1.33

calculate out/in-degree distributions ...
[out] {max,avg,E[X^2],var,stddev} is 3 1.33333 2.44444 0.666667 0.816497
[in ] {max,avg,E[X^2],var,stddev} is 3 1.33333 2.44444 0.666667 0.816497
done. (0.00 msec.)

compute CM.BC, CM.eBC,
found 9 vertices
random sampling 9 sources from 9 nodes
finished source sampling (0.01 msec.)

NUMA Node 00 warmed up.

--------------------------------------------------------------------------------------------
[#ites]   id  u/w  k  s  trE  hops (max, id)     [ms] (sp:up)     TEPS    elapsed  remain (h:m:s)
--------------------------------------------------------------------------------------------
[9/9] [002] [u]  0  3   2   2 ( 3, s=2)    0.0 ( 0%,100%)  9.8e+06      0.000  (00:00:00.000)
[1/9] [001] [u]  0  8   0   0 ( 0, s=1)    0.0 (100%, 0%)  6.1e+06      0.000  (00:00:00.001)
[NUMA local merging] 0 { 0-0 } <= 1 { 0-1 }
[NUMA local merging] 0 { 0-0 } <= 2 { 0-2 }
[NUMA local merging] 0 { 0-0 } <= 3 { 0-3 }
[NUMA merging] 2 { 0-2 } <= 0 { 0-0 } n: 5-7
[NUMA merging] 3 { 0-3 } <= 0 { 0-0 } n: 7-9
[NUMA merging] 1 { 0-1 } <= 0 { 0-0 } n: 3-5
[NUMA merging] 0 { 0-0 } <= 0 { 0-0 } n: 0-3
[NUMA merging] 0 { 0-0 } <= 0 { 0-0 } m: 0-3
[NUMA merging] 2 { 0-2 } <= 0 { 0-0 } m: 6-9
[NUMA merging] 3 { 0-3 } <= 0 { 0-0 } m: 9-12
[NUMA merging] 1 { 0-1 } <= 0 { 0-0 } m: 3-6

finished computing centrality (0.48 msec.)


----------------------------------------------------------------------
NETAL (NETwork Analysis Library)
Version: NETAL 1.60 @ 2016-09-23
Copyright: Copyright (C) 2011-2016 Yuichiro Yasui
----------------------------------------------------------------------
Machine: 1 sockets (4 cores), 0.00 GB RAM
----------------------------------------------------------------------
graph file     is data/sample.gr dimacs(0)
dump file      is out
kernel         is centrality (uB, acc=100.000000%, unweighted, random sampling)
graph          is n=9(log2{n}=3.17), m=12(log2{m}=3.58), m/n=1.33
diameter       is 3 (s=2)
out-degree     is {max,avg,E[X^2],var,stddev} is 3 1.33333 2.44444 0.666667 0.816497
in-degree      is {max,avg,E[X^2],var,stddev} is 3 1.33333 2.44444 0.666667 0.816497
traversal      is 34 nodes and 32 edges
cpu time       is 0.001 seconds (00:00:00.00) (#Threads=4)
profile        is spaths: 0.001ms (0.090%), update: 0.001ms (0.056%)


% cat out
c file name: out
c instance name: sample.gr
c created: Mon Nov 14 23:52:40 2016
c
p sp 9 12
c
c centrality (uB, acc=100.000000%, unweighted, random sampling) (|Vs|=9, exact)
c diameter is 3 (s=2)
c
l Betweenness
L edge Betweenness
c
d 1 0
d 2 2
d 3 2.83333
d 4 2.16667
d 5 5
d 6 2
d 7 2.16667
d 8 0
d 9 0.833333
e 1 2 3
e 1 3 1.83333
e 1 4 3.16667
e 2 5 7
e 3 7 4.83333
e 4 7 2.33333
e 4 9 2.83333
e 5 3 4
e 5 6 5
e 6 8 3
e 7 8 3.16667
e 9 8 1.83333
```








Graph processing using NETAL
----------------------------

#### Centrality

We assume that an input graph G = (V, A), based on an adjacency vertex list A represents a directed graph, where an adjacency list A(v) contains the adjacency vertices w of outgoing edges (v,w) in E for each vertex v in V.
The centrality metric is defined by using shortest paths as follows: first, for the directed graph G=(V,A) and s, t in V, denote d_G(s,t) as the shortest (s,t)-path distance.
Let sigma(s,t) be the number of shortest (s,t)-paths, and sigma(s,t|v) be the number of shortest (s,t)-paths that contains v.
If s=t, let \sigma(s,t) = 1, and if v in {s,t}, let sigma(s,t|v) = 0.

The centrality analysis with NETAL specifies target centrality metrics and their accuracy, using the following syntax;

 `-z cent  <Identifiers> <accuracy>` 
```
% ./netal -i data/sample.gr -t dimacs -z cent uB 1.00 -o out
                                              ~~ ~~~~
```

, where the Identifiers are described by a character in the second column on following table and the accuracy is specified by a number in [0.0, 1.0] for target nodes.

For example, `CGSBum` indicates that NETAL computes Closeness, Graph, Betweenness, and edge-Betweenness based on unweighted shortest paths for nodes and edges on the maximum component.

Both Efficiency and Straightness require a coordinate (x, y) for each node, and specify the DIMACS-format coordinate file by the environment value `DIMACS_CO`. More detail of DIMACS coordinate file is described on [^dimacs].

| Name                     | Identifier | Definition                                            |  
|:-------------------------|:-----------|:------------------------------------------------------|
| *Closeness*[^CC]         | `C`        | CC(v) = 1 / sum_{t in V}{dist(v,t)}                  |
| *Graph*[^GC]             | `G`        | GC(v) = 1 / max_{t in V}{dist(v,t)}                  |
| *Stress*[^SC]            | `S`        | SC(v) = sum_{s, t in V}{sigma(s, t｜v)}               |
| *Betweenness*[^BC]       | `B`        | BC(v) = sum_{s, t in V}{sigma(s, t｜v) / sigma(s, t)} |
| *edge-Betweenness*[^eBC] | `B`        | BC(e) = sum_{s, t in V}{sigma(s, t｜e) / sigma(s, t)} |
| *Degree*[^DC]            | `D`        | DC(v) = sum_{v in V}{deg_G(v)}                        |
| *Efficiency*[^EC]        | `E`        | EC(v) = sum_{t in V}{1/dist(v,t)} / sum_{t in V}{1/dist_eucl(i,j)} |
| *Straightness*[^TC]      | `T`        | TC(v) =  sum_{t in V}{dist_eucl(v,t)/dist(v,t)} / (N-1) |

| Name              | Identifier | Definition                                                  |  
|:------------------|:-----------|:------------------------------------------------------------|
| *weighted*        | `w`        | use weighted shortest paths using Dijstra's algorithm       |
| *unweighted*      | `u`        | use unweighted shortest paths using Breadth-first search    |
| *max-CC*          | `m`        | compute centrality for a maximum component only             |


 





Performance of NETAL
--------------------

Tables shows the performance of NETAL for centrality computation for graph instances on 4-way Opteron 6174 system with 48 threads based on CPU and memory affinity using ULIBC.

| Graph              |       n |        m | diam(G) | NETAL (CC, GC, SC, BC) | GraphCT (BC)  |
|:-------------------|:--------|:---------|--------:|---------------:|---------:|
|`USA-road-d.NY`     |   264 K |    734 K |     720 |         0.11 h |   3.66 h |
|`USA-road-d.LKS`    |  2.76 M |   6.89 M |   4,127 |        19.35 h | 493.77 h |
|`web-BerkStan`      |   685 K |   7.60 M |     714 |         0.66 h |  17.99 h |
|`web-Google`        |   876 K |   5.12 M |      51 |         2.15 h |  52.97 h |
|`Wiki-Talk`         |  2.39 M |   5.02 M |      11 |         2.05 h |  22.10 h |
|`cit-Patents`       |  3.78 M |  16.52 M |      24 |         1.87 h |  23.61 h |








File Formats
------------
The NETAL supports three graph file formats, such as 9th-DIMACS[^dimacs], SNAP[^snap], METIS[^metis] (partial support).

### 9th-DIMACS Graph file Format
The prefix “`c`” means that this line is comment line.

###### Number of vertices and edges
The `p` line describes the number of nodes `n` and number of edge `m`.
```
p sp n m
```
e.g.) `p sp 4 5`: The input graph contains 4 vertices and 5 edges.

###### Edge (weighted only)
The `a` line describes a weighted edge (`u`, `v`) that has a weight value `l`.

```
a u v l
```

e.g.) `a 1 2 3`: The input graph contains edge (1,2) that has a weight 3.

### SNAP Graph file format

The prefix “`#`” means that this line is comment line.

Each line describes an unweighted edge (`u`, `v`).

```
u v
```

e.g.) `1 2`: The input graph contains the edge (1,2).


### METIS Graph file format (partial support)

The prefix “`%`” means that this line is comment line.

###### Unweighted graph

The first line describes the number of nodes `n` and number of edge `m`.

```
n m
```

or

```
n m 0
```

On second line or later (n lines), the i-th line (excluding comment lines) contains adjacent vertices `w1`, `w2`, `w3`, ... of i-th vertex.

```
w1 w2 w3 ...
```

###### Weighted graph

The first line describes the number of nodes `n` and number of edge `m`.

```
n m 10
```

On second line or later (n lines), the i-th line (excluding comment lines) contains pairs of adjacent vertices `w1`, `w2`, `w3`, ... of i-th vertex and their edge weights `l1`, `l2`, `l3`, ....

```
w1 l1 w2 l2 w3 l3 ...
```

### NETAL Output File Format

###### Number of vertices and edges

The `p` line describes the number of nodes `n` and number of edge `m`.

```
p sp n m
```

e.g.) `p sp 9 12`: The input graph contains 9 nodes and 12 edges.

###### Vertex metric name

The i-th vertex metric name line (excluding comment lines) describes the metric name of i-th column in each vertex metric line.

```
l NAME
```

e.g.) `l betweenness` : i-th column of vertex metric line is a value of betweenness centrality.

###### Edge metric name

The i-th edge metric name line (excluding comment lines) describes the metric name of i-th column in each edge metric line.

```
L NAME
```

e.g.) `L edge-betweenness` : i-th column of edge metric line is `edge-betweenness`

### Vertex metric

The vertex metric line describes the metrics *val1*, *val2*, ..., of vertex v.

```
d v val1 val2 ...
```

e.g.) `d 3 2.83333`: a metric of node 3 is 2.83333.

### Edge metric

The edge metric line describes the metrics *val1*, *val2*, ... of edge
$(u,v)$.

```
e u v val1 val2 ...
```

e.g.) `e 4 7 2.33333`: a value for arc (4,13) is 325,361.








References
----------
[^dimacs]: <http://www.dis.uniroma1.it/challenge9/format.shtml#graph>
[^snap]: <http://snap.stanford.edu/data/index.html>
[^metis]: <http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/manual.pdf>
[^CC]: G. Sabidussi: The centrality index of a graph. Psychometrika, 31:581-603. (1966)
[^GC]: P. Hage and F. Harary: Eccentricity and centrality in networks. Social Networks, 17:57-63. (1995)
[^SC]: A. Shimbel: Structural parameters of communication networks. Bulletin of Mathematical Biophysics, 15:501-507. (1953)
[^BC]: L. C. Freeman: A set of measures of centrality based on betweenness. Sociometry, 40:35-41. (1977)
[^eBC]: U. Brandes: On Variants of Shortest-Path Betweenness Centrality and their Generic Computation, Social Networks 30(2) (2008)
[^DC]: L. C. Freeman: Centrality in social networks conceptual clarification Social Networks, Vol. 1, Issue 3, 215-239. (1979)
[^EC]: V. Latora, and M. Massimo: Efficient Behavior of Small-World Networks, Physical Review Letters, vol. 87, Issue 19, (2001).
[^TC]: I. Vragović, E. Louis, and Díaz-Guilera: Efficiency of informational transfer in regular and complex networks, Physical Review E 71, 036122 (2005)