#ifndef STAT_H
#define STAT_H

#if defined (__cplusplus)
extern "C" {
#endif
  void output_results(const I64_t ites,
		      const double *sssp_times, const I64_t *trav_edges);
#if defined (__cplusplus)
}
#endif

#endif /* STAT_H */
