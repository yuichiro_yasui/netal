#ifndef COMPORNENT_H
#define COMPORNENT_H

struct comp_graph_t {
  I64_t n;
  I64_t m;
  I64_t *compid;
  I64_t num_comps;
  struct comp_t {
    I64_t root, elements;
  } *comps;
  graph **subg;
};

#if defined (__cplusplus)
extern "C" {
#endif
  struct profile_t make_colors(graph *G);
  struct comp_graph_t *construct_comp_graph(graph *G, int top);
  void free_comp_graph(struct comp_graph_t *cg);
  void dump_colors(char *filename, char *desc, struct comp_graph_t *cg);
#if defined (__cplusplus)
}
#endif

#endif /* COMPORNENT_H */
