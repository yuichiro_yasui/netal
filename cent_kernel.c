#include <stdio.h>
#include <math.h>
#include <time.h>

#include <ulibc.h>
#include <omp_helpers.h>

#include "defs.h"
#include "utls.h"
#include "graph.h"
#include "cent_kernel.h"
#include "cent_variant.h"
#include "rand_sampling.h"
#include "heap.h"
#include "sssp.h"

/* ------------------------------------------------------------ *
 * interface
 * ------------------------------------------------------------ */
static struct profile_t compute_kernel(graph *G_addr, int64_t *S_addr,
				       struct cent_param_t *params,
				       struct cent_metric_t *metrics);

struct profile_t compute_cent_kernel(graph *G, struct cent_param_t *params, struct cent_metric_t *metrics) {
  double init_time, comp_time;
  int64_t *roots = NULL;

  /* source sampling */
  PROFILED( init_time, roots = make_roots(params->sequence, G, &params->num_srcs) );
  printf("finished source sampling (%.2f msec.)\n", init_time);
  printf("\n");

  /* thread-parallel computation */
  struct profile_t prof;
  PROFILED( comp_time, prof = compute_kernel(G, roots, params, metrics) );
  printf("finished computing centrality (%.2f msec.)\n", comp_time);
  printf("\n");

  free(roots);
  
  return prof;
}

void dump_cent_metrics(char *filename, char *desc, char *kernel,
		       graph *G, struct cent_param_t *params,
		       struct cent_metric_t *metrics) {
  if (!filename) {
    return;
  }
  SPINT i, j;
  
  FILE *fp = fopen(filename, "w");
  fprintf(fp, "c file name: %s\n", filename);
  if (desc) {
    fprintf(fp, "c instance name: %s\n", desc);
  }
  
  time_t now = time(NULL);
  fprintf(fp, "c created: %s", ctime(&now));
  fprintf(fp, "c\n");
  fprintf(fp, "p sp %" PRId64 " %" PRId64 "\n", G->n, G->m);
  fprintf(fp, "c\n");

  if (!params->hop_bound) {
    fprintf(fp, "c %s (|Vs|=%lld, %s)\n",
	    kernel, (I64_t)params->num_srcs,
	    (G->n == params->num_srcs) ? "exact" : "random-sampling");
  } else {
    fprintf(fp, "c %lld-%s (|Vs|=%lld, %s)\n",
	    (I64_t)params->hop_bound, kernel, (I64_t)params->num_srcs,
	    (G->n == params->num_srcs) ? "exact" : "random-sampling");
  }

  fprintf(fp, "c diameter is %" PRId64 " (s=%" PRId64 ")\n",
 metrics->diam_hops, metrics->diam_id+1);
  
  fprintf(fp, "c\n");
  if (metrics->CC)  { fprintf(fp, "l Closeness\n"); }
  if (metrics->DC)  { fprintf(fp, "l Degree\n"); }
  if (metrics->EC)  { fprintf(fp, "l Efficiency\n"); }
  if (metrics->GC)  { fprintf(fp, "l Graph\n"); }
  if (metrics->TC)  { fprintf(fp, "l Straightness\n"); }
  if (metrics->BC)  { fprintf(fp, "l Betweenness\n"); }
  if (metrics->SC)  { fprintf(fp, "l Stress\n"); }
  if (metrics->eBC) { fprintf(fp, "L edge Betweenness\n"); }
  if (metrics->eSC) { fprintf(fp, "L edge Stress\n"); }
  fprintf(fp, "c\n");
  
  if ( metrics->CC || metrics->DC || metrics->EC || metrics->GC ||
       metrics->TC || metrics->BC || metrics->SC) {
    for (i = 0; i < G->n; ++i) {
      fprintf(fp, "d %lld", (I64_t)i+1);
      if (metrics->CC) fprintf(fp, " %g", metrics->CC[i]);
      if (metrics->DC) fprintf(fp, " %g", metrics->DC[i]);
      if (metrics->EC) fprintf(fp, " %g", metrics->EC[i]);
      if (metrics->GC) fprintf(fp, " %g", metrics->GC[i]);
      if (metrics->TC) fprintf(fp, " %g", metrics->TC[i]);
      if (metrics->BC) fprintf(fp, " %g", metrics->BC[i]);
      if (metrics->SC) fprintf(fp, " %g", metrics->SC[i]);
      fprintf(fp, "\n");
    }
  }
  if ( metrics->eBC || metrics->eSC ) {
    for (i = 0; i < G->n; ++i) {
      for (j = G->forward[i]; j < G->forward[i+1]; ++j) {
	fprintf(fp, "e %lld %lld", (I64_t)i+1, (I64_t)G->head[j]+1);
	if (metrics->eBC) fprintf(fp, " %g", metrics->eBC[j]);
	if (metrics->eSC) fprintf(fp, " %g", metrics->eSC[j]);
	fprintf(fp, "\n");
      }
    }
  }
  
  fclose(fp);
}




/* ------------------------------------------------------------ *
 * compute_kernel
 * ------------------------------------------------------------ */
struct cent_tmp_t {
  /* variables (1) */
  node_t *ND;
  heap_t *HQ;
  SPINT *ds;
  SPINT *hops;
  succ_t *succ_list;
  SPINT *succ_count;
  SPINT *queue;
  double *sigma;
  double *delta;
  
  /* variables (2) */
  double *BC;
  double *eBC;
  double *SC;
  double *eSC;
};


static struct cent_result_t compute_vertex_iter(struct cent_param_t *params,
						graph *G, SPINT s,
						struct cent_tmp_t *tmp,
						struct cent_metric_t *metrics);
void update_maxhops_node(struct cent_result_t r);
void print_titlebar(long n, long m, long k);
void print_vertex_iter(graph *G, struct cent_param_t *params,
		       struct cent_result_t result, int64_t itenumber);
static struct cent_tmp_t *alloc_cent_temps(graph *G,
					   struct cent_param_t *params,
					   struct cent_metric_t *metrics);
static void free_cent_temps(struct cent_param_t *params,
			    struct cent_metric_t *metrics, struct cent_tmp_t *tmp);
static void merge_indices(SPINT n, SPINT m, struct cent_metric_t *metrics);

/* NUMA node local variables */
static unsigned char *pool[MAX_NODES] = { NULL };

/* thread local variables */
static struct cent_metric_t tls_metrics[MAX_THREADS];
static struct nodehop_t { int64_t hops, index; } tls_nodehop[MAX_THREADS] = {{ 0, -1 }};

static struct profile_t compute_kernel(graph *G_addr, int64_t *S_addr,
				       struct cent_param_t *params,
				       struct cent_metric_t *metrics) {
  struct cent_result_t *results = NULL;
  results = calloc(params->num_srcs, sizeof(struct cent_result_t));
  assert(results);

  set_timer( 1.0, params->time_limit );
  set_culling( 20 );

  _Pragma("omp parallel") {
    ULIBC_bind_thread_explicit( ULIBC_get_thread_num() );
    struct numainfo_t loc = ULIBC_get_numainfo( ULIBC_get_thread_num() );
    if ( loc.core == 0 ) {
      pool[loc.node] = (loc.node == 0) ?
	(unsigned char *)G_addr : localcopy_graph(G_addr, loc.node);
      printf("NUMA Node %02d warmed up.\n", loc.node);
    }
  }
  printf("\n");

  int64_t shared_index = params->startid;
  
  _Pragma("omp parallel") {
    ULIBC_bind_thread_explicit( ULIBC_get_thread_num() );
    struct numainfo_t loc = ULIBC_get_numainfo( ULIBC_get_thread_num() );
    
    /* allocate thread local variables */
    graph *G = (graph *)pool[loc.node];
    struct cent_tmp_t *tmp = alloc_cent_temps(G, params, metrics);

    /* main loop */
    while (1) {
      int64_t start, length;
      _Pragma("omp critical") {
	start = shared_index;
	length = (params->startid + params->num_srcs - start > 512) ? 8 : 1;
	shared_index += length;
      }
      if (start >= params->num_srcs + params->startid) break;
    
      for (int64_t i = start; i < start + length; ++i) {
	const int64_t s = S_addr[i]; /* root */
	struct cent_result_t r = compute_vertex_iter(params, G, s, tmp, metrics);
	memcpy(&results[i-params->startid], &r, sizeof(struct cent_result_t));
	update_maxhops_node(r);
	print_vertex_iter(G, params, r, i);
      }
      
      if ( timelimit() < 0 ) break;
    }
    _Pragma("omp barrier");

    if (params->verbose)
      printf("%d-th thread finished\n", loc.id);
    _Pragma("omp barrier");

    /* NUMA local/remote merging */
    merge_indices(G_addr->n, G_addr->m, metrics);
    _Pragma("omp barrier");

    if (params->verbose)
      printf("%d-th thread finished\n", loc.id);
    _Pragma("omp barrier");
    
    /* free thread local variables */
    free_cent_temps(params, metrics, tmp);
  }
  printf("\n");

  metrics->diam_hops = 0;
  metrics->diam_id = -1;
  for (int id = 0; id < ULIBC_get_num_threads(); ++id) {
    if (metrics->diam_hops < tls_nodehop[id].hops) {
      metrics->diam_hops = tls_nodehop[id].hops;
      metrics->diam_id = tls_nodehop[id].index;
    }
  }

  /* free NUMA local graphs */
  for (int i = 0; i < ULIBC_get_online_nodes(); ++i) {
    NUMA_free(pool[i]);
  }

  struct cent_result_t total = {
    .s = -1,
    .trav_nodes = 0,
    .trav_edges = 0,
    .max_hops   = 0,
    .sssp_ms    = 0.0,
    .update_ms  = 0.0
  };
  for (int64_t i = 0; i < params->num_srcs; ++i) {
    if (total.max_hops < results[i].max_hops) {
      total.s = results[i].s;
      total.max_hops = results[i].max_hops;
    }
    total.trav_nodes += results[i].trav_nodes;
    total.trav_edges += results[i].trav_edges;
    total.sssp_ms    += results[i].sssp_ms;
    total.update_ms  += results[i].update_ms;
  }

  struct profile_t prof = {
    .iterations = params->num_srcs,
    .total = total,
    .results = results,
  };
  return prof;
}

void update_maxhops_node(struct cent_result_t r) {
  struct numainfo_t loc = ULIBC_get_numainfo( ULIBC_get_thread_num() );
  if (tls_nodehop[loc.id].hops < r.max_hops) {
    tls_nodehop[loc.id].hops = r.max_hops;
    tls_nodehop[loc.id].index = r.s;
  }
}

void print_titlebar(long n, long m, long k) {
  const int wd_n = intlog(10,n), wd_m = intlog(10,m), wd_k = intlog(10,k);
  const int wd_h = intlog(10, intsqrt(n)) + 1;
  const int width = wd_n*4 + wd_h*2 + wd_m + wd_k + 81;
  const char *bar = set_nsymb(alloca(width+1), '-', width);
  printf("%s\n[%*s]   id  u/w  %*s  %*s  %*s  %*s  %*s  %*s  %*s  %*s\n%s\n",
	 bar,
	 wd_n*2+1, "#ites",
	 wd_k, "k", wd_n, "s", wd_m, "trE",
	 wd_h*2+wd_n+7, "hops (max, id)", 
	 15, "[ms] (sp:up)", 7, "TEPS", 9, "elapsed", 12, "remain (h:m:s)",
	 bar);
}

void print_vertex_iter(graph *G, struct cent_param_t *params,
		       struct cent_result_t result, int64_t itenumber) {
  if ( !(params->verbose || 
	 itenumber-params->startid == 0 ||
	 itenumber-params->startid == params->num_srcs-1 ||
	 timelimit()) ) return;
  
  const int wd_i = intlog(10,params->num_srcs);
  const int wd_n = intlog(10,G->n), wd_m = intlog(10,G->m);
  const int wd_h = intlog(10,intsqrt(G->n)) + 1;
  const double crr_time = result.sssp_ms + result.update_ms;
  const double crr_teps = 1.0 * G->m / crr_time * 1e-3;
  const double elapsed = toc() * 1e-3;
  const double term = elapsed * params->num_srcs / (itenumber+1);
  const double remain = term - elapsed;
  const int hh = remain / 3600;
  const int mm = (remain - 3600 * hh) / 60;
  const double ss = remain - 3600 * hh - 60 * mm;

  if ( culling() ) print_titlebar(G->n, G->m, params->hop_bound);

  struct numainfo_t loc = ULIBC_get_numainfo( ULIBC_get_thread_num() );
  struct nodehop_t nh = tls_nodehop[loc.id];
  
  printf("[%0*" PRId64 "/%" PRId64 "] [%03d]"
	 " [%c]  %" PRId64 "  "
	 "%*" PRId64 "  %*" PRId64 "  %*" PRId64 " (%*" PRId64 ", s=%*" PRId64 ")  "
	 "%5.1f (%2.0f%%,%2.0f%%)  %.1e  "
	 "%9.3f  (%02d:%02d:%06.3f)"
	 "\n",
	 wd_i, itenumber+1, params->num_srcs, loc.id,
	 (params->weighted ? 'w' : 'u'), params->hop_bound,
	 wd_n, result.s+1, wd_m, result.trav_edges,
	 wd_h, result.max_hops, wd_h, nh.hops, wd_n, nh.index+1, 
	 crr_time, result.sssp_ms/(crr_time)*100, result.update_ms/(crr_time)*100,
	 crr_teps*1e6,
	 elapsed, hh, mm, ss);
}

static struct cent_tmp_t *alloc_cent_temps(graph *G,
					   struct cent_param_t *params,
					   struct cent_metric_t *metrics) {
  struct cent_tmp_t *tmp = (struct cent_tmp_t *)malloc(sizeof(struct cent_tmp_t));
  assert( tmp );
  
  tmp->ND = (node_t *)malloc(sizeof(node_t) * G->n);
  tmp->HQ = (heap_t *)malloc(sizeof(heap_t) * G->n);
  assert( tmp->ND && tmp->HQ );
  
  if (params->hop_bound) {
    tmp->ds   = (SPINT *)malloc(sizeof(SPINT) * G->n);
    tmp->hops = (SPINT *)tmp->ds;
  } else {
    tmp->ds   = (SPINT *)tmp->ND; /* reuse */
    tmp->hops = (SPINT *)tmp->ND;
  }
  assert( tmp->ds && tmp->hops );
  
  tmp->queue      = (SPINT  *)malloc(sizeof(SPINT) * (G->n+1));
  assert( tmp->queue );
  
  tmp->succ_list  = (succ_t *)malloc(sizeof(succ_t) * G->m); /* m */
  tmp->succ_count = (SPINT  *)malloc(sizeof(SPINT)  * G->n);
  tmp->sigma      = (double *)malloc(sizeof(double) * G->n);
  tmp->delta      = (double *)tmp->HQ; /* reuse */
  assert( tmp->succ_list && tmp->succ_count && tmp->sigma );
  
  if ( metrics->BC || metrics->eBC ) {
    const int id = ULIBC_get_thread_num();
    tls_metrics[id].BC = (double *)calloc(G->n, sizeof(double));
    tls_metrics[id].eBC = (double *)calloc(G->m, sizeof(double));
    assert( tls_metrics[id].BC && tls_metrics[id].eBC );
  }
  if ( metrics->SC || metrics->eSC ) {
    const int id = ULIBC_get_thread_num();
    tls_metrics[id].SC = (double *)calloc(G->n, sizeof(double));
    tls_metrics[id].eSC = (double *)calloc(G->m, sizeof(double));
    assert( tls_metrics[id].SC && tls_metrics[id].eSC );
  }
  return tmp;
}


static void free_cent_temps(struct cent_param_t *params,
			    struct cent_metric_t *metrics, struct cent_tmp_t *tmp) {
  free( tmp->ND );
  free( tmp->HQ );
  if (params->hop_bound) {
    free( tmp->ds );
  }
  free( tmp->queue );
  free( tmp->succ_list );
  free( tmp->succ_count );
  free( tmp->sigma );
  free( tmp );
  if ( metrics->BC || metrics->eBC ) {
    const int id = ULIBC_get_thread_num();
    free( tls_metrics[id].BC );
    free( tls_metrics[id].eBC );
  }
  if ( metrics->SC || metrics->eSC ) {
    const int id = ULIBC_get_thread_num();
    free( tls_metrics[id].SC );
    free( tls_metrics[id].eSC );
  }
}


static void merge_indices(SPINT n, SPINT m, struct cent_metric_t *metrics) {
  struct numainfo_t loc = ULIBC_get_numainfo( ULIBC_get_thread_num() );
  /* master NUMA core */
  if ( loc.core == 0 ) {
    for (int id = 0; id < ULIBC_get_num_threads(); ++id) {
      struct numainfo_t loc2 = ULIBC_get_numainfo( id );
      if ( loc.node == loc2.node && loc2.core != 0 ) {
	printf("[NUMA local merging] %d { %d-%d } <= %d { %d-%d }\n",
	       loc.id, loc.node, loc.core, loc2.id, loc2.node, loc2.core);
	struct cent_metric_t *dst = &tls_metrics[loc.id];
	struct cent_metric_t *src = &tls_metrics[loc2.id];
	if ( metrics->SC ) {
	  for (int64_t j = 0; j < n; ++j) dst-> SC[j] += src-> SC[j];
	  for (int64_t j = 0; j < m; ++j) dst->eSC[j] += src->eSC[j];
	}
	if ( metrics->BC ) {
	  for (int64_t j = 0; j < n; ++j) dst-> BC[j] += src-> BC[j];
	  for (int64_t j = 0; j < m; ++j) dst->eBC[j] += src->eBC[j];
	}
      }
    }
  }
  _Pragma("omp barrier");

  /* master NUMA node */
  if ( loc.node == 0 ) {
    if ( metrics->SC ) {
      if ( loc.core == 0 ) {
	memset(metrics->SC, 0, sizeof(double) * (n));
	memset(metrics->eSC, 0, sizeof(double) * (m));
      }
      ULIBC_node_barrier();
      for (int i = 0; i < ULIBC_get_num_threads(); ++i) {
	struct numainfo_t loc2 = ULIBC_get_numainfo( i );
	if ( loc2.core != 0 ) continue;
	int64_t ls, le;
	prange(n, 0, loc.lnp, loc.core, &ls, &le);
	for (int64_t j = ls; j < le; ++j) metrics->SC[j] += tls_metrics[i].SC[j];
	prange(m, 0, loc.lnp, loc.core, &ls, &le);
	for (int64_t j = ls; j < le; ++j) metrics->eSC[j] += tls_metrics[i].eSC[j];
      }
    }
    if ( metrics->BC ) {
      if ( loc.core == 0 ) {
	memset(metrics->BC, 0, sizeof(double) * (n));
	memset(metrics->eBC, 0, sizeof(double) * (m));
      }
      ULIBC_node_barrier();
      for (int i = 0; i < ULIBC_get_num_threads(); ++i) {
	struct numainfo_t loc2 = ULIBC_get_numainfo( i );
	if ( loc2.core != 0 ) continue;
	int64_t ls, le;
	prange(n, 0, loc.lnp, loc.core, &ls, &le);
	for (int64_t j = ls; j < le; ++j) metrics->BC[j] += tls_metrics[i].BC[j];
	printf("[NUMA merging] "
	       "%d { %d-%d } <= %d { %d-%d } n: %" PRId64 "-%" PRId64 "\n",
	       loc.id, loc.node, loc.core, loc2.id, loc2.node, loc2.core, ls, le);
	ULIBC_node_barrier();
	prange(m, 0, loc.lnp, loc.core, &ls, &le);
	for (int64_t j = ls; j < le; ++j) metrics->eBC[j] += tls_metrics[i].eBC[j];
	printf("[NUMA merging] "
	       "%d { %d-%d } <= %d { %d-%d } m: %" PRId64 "-%" PRId64 "\n",
	       loc.id, loc.node, loc.core, loc2.id, loc2.node, loc2.core, ls, le);
      }
    }
  }
}



/* ------------------------------------------------------------ *
 * compute_vertex_iter
 * ------------------------------------------------------------ */
static struct cent_result_t compute_vertex_iter(struct cent_param_t *params,
						graph *G, SPINT s,
						struct cent_tmp_t *tmp,
						struct cent_metric_t *metrics) {
  struct cent_result_t R;
  memset(&R, 0x00, sizeof(struct cent_result_t));
  result_t r;
  memset(&r, 0x00, sizeof(result_t));
  double t1, t2;
  SPINT j, k, v;
  
  /* shortest-paths */
  t1 = get_msecs();
  if ( metrics->CC || metrics->EC || metrics->GC || metrics->TC ||
       metrics->BC || metrics->SC ) {
    if (params->hop_bound) {
      /* k-bound shortest-paths */
      if (params->weighted) {
	memset((void *)tmp->ND, 0xff, G->n * sizeof(node_t));
	r = MP_kSSSP_dijkstra(G->forward, G->head, G->length, s,
			      tmp->ND, tmp->HQ, tmp->succ_list, tmp->succ_count,
			      tmp->queue, tmp->sigma, params->hop_bound, tmp->hops);
      } else {
	memset((void *)tmp->ds, 0xff, G->n * sizeof(SPINT));
	r = MP_kBFS(G->forward, G->head, s,
		    tmp->ds, tmp->succ_list, tmp->succ_count,
		    tmp->queue, tmp->sigma, params->hop_bound);
      }
    } else {
      /* shortest-paths */
      if (params->weighted) {
	memset((void *)tmp->ND, 0xff, sizeof(node_t) * G->n);
	r = MP_SSSP_dijkstra(G->forward, G->head, G->length, s,
			     tmp->ND, tmp->HQ, tmp->succ_list, tmp->succ_count,
			     tmp->queue, tmp->sigma);
      } else {
	memset((void *)tmp->ds, 0xff, G->n * sizeof(SPINT));
	r = MP_BFS(G->forward, G->head, s,
		   tmp->ds, tmp->succ_list, tmp->succ_count,
		   tmp->queue, tmp->sigma);
      }
    }
  }
  t2 = get_msecs();
  R.trav_nodes = r.trav_nodes;
  R.trav_edges = r.trav_edges;
  R.sssp_ms = t2 - t1;
  R.s = s;
 
  /* update centrality metrics */
  t1 = get_msecs();
  
  /* update DC */
  if ( metrics->DC ) {
    I64_t deg = G->forward[s+1] - G->forward[s];
    metrics->DC[s] = (double)deg * DC_BASE(G->n);
  }

  /* update CC, GC */
  if ( metrics->CC && r.CC != 0.0 ) {
    metrics->CC[s] = CC_BASE(G->n) / r.CC;
  }
  if ( metrics->GC && r.GC != 0.0 ) {
    metrics->GC[s] = GC_BASE(G->n) / r.GC;
  }
  R.max_hops = r.GC;


  /* update EC, TC */
  if ( G->X && G->Y && ( metrics->EC || metrics->TC ) ) {
    I64_t v, dist_v;
    double TC_s = 0.0, EC_s = 0.0;
    double eucl_s_v, eucl_s = 0.0;
    double X_s = G->X[s], Y_s = G->Y[s];
    for (v = 0; v < G->n; ++v) {
      eucl_s_v = fsqrt( (X_s - G->X[v]) * (X_s - G->X[v]) + 
			(Y_s - G->Y[v]) * (Y_s - G->Y[v]) );
      if ( eucl_s_v != 0.0 ) {
	eucl_s += 1.0 / eucl_s_v;
      }
      if (params->weighted) {
	dist_v = tmp->ND[v].key;
      } else {
	dist_v = tmp->ds[v];
      }
      if ( dist_v > 0 ) {
	EC_s += 1.0 / dist_v;
    	TC_s += eucl_s_v / dist_v;
      }
    }
    if (metrics->EC && eucl_s != 0.0) {
      metrics->EC[s] = EC_BASE(G->n) * EC_s / eucl_s;
    }
    if (metrics->TC) {
      metrics->TC[s] = TC_s * TC_BASE(G->n);
    }
  }
 
  /* update BC */
  const int id = ULIBC_get_thread_num();
  
  if ( tls_metrics[id].BC && tls_metrics[id].eBC && metrics->BC && metrics->eBC ) {
    SPINT *forward = G->forward;
    double *BC = tls_metrics[id].BC;
    double *eBC = tls_metrics[id].eBC;
    double sigma_v, lbc;
    succ_t *p;
    for (j = 0; j < G->n; ++j) {
      tmp->delta[j] = 0.0;
    }
    for (j = R.trav_nodes-1; j > 0; --j) {
      v =  tmp->queue[j];
      p = &tmp->succ_list[ forward[v] ];
      sigma_v = tmp->sigma[v];
      for (k = tmp->succ_count[v] - forward[v]; k > 0; --k, ++p) {
    	lbc = sigma_v * (tmp->delta[p->w] + 1.0) / tmp->sigma[p->w];
    	tmp->delta[v] += lbc;
    	eBC[p->v_w] += lbc * BC_BASE(G->n);
      }
      BC[v] += tmp->delta[v] * BC_BASE(G->n);
    }
    v =  tmp->queue[0];
    p = &tmp->succ_list[ forward[v] ];
    sigma_v = tmp->sigma[v];
    for (k = tmp->succ_count[v] - forward[v]; k > 0; --k, ++p) {
      lbc = sigma_v * (tmp->delta[p->w] + 1.0) / tmp->sigma[p->w];
      eBC[p->v_w] += lbc * BC_BASE(G->n);
    }
  }
  
  /* update SC */
  if ( tls_metrics[id].SC && tls_metrics[id].eSC && metrics->SC && metrics->eSC ) {
    SPINT *forward = G->forward;
    double *SC = tls_metrics[id].SC;
    double *eSC = tls_metrics[id].eSC;
    succ_t *p;
    for (j = 0; j < G->n; ++j) {
      tmp->delta[j] = 0.0;
    }
    for (j = R.trav_nodes-1; j > 0; --j) {
      v = tmp->queue[j];
      p = &tmp->succ_list[ forward[v] ];
      for (k = tmp->succ_count[v] - forward[v]; k > 0; --k, ++p) {
    	tmp->delta[v] += tmp->delta[p->w] + 1.0;
    	eSC[ p->v_w ] += (tmp->delta[p->w] + 1.0) * SC_BASE(G->n);
      }
      SC[v] += tmp->delta[v] * tmp->sigma[v] * SC_BASE(G->n);
    }
    v = tmp->queue[0];
    p = &tmp->succ_list[ forward[v] ];
    for (k = tmp->succ_count[v] - forward[v]; k > 0; --k, ++p) {
      eSC[ p->v_w ] += (tmp->delta[p->w] + 1.0) * SC_BASE(G->n);
    }
  }
  
  t2 = get_msecs();
  R.update_ms = t2 - t1;

  return R;
}
