#ifndef HEAP_H
#define HEAP_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

typedef struct {
  I64_t trav_nodes;
  I64_t trav_edges;
  double CC;
  double GC;
} result_t;

typedef struct {
  I64_t key, id;
} heap_t;

typedef struct {
  I64_t key, offset;
} node_t;

typedef struct {
  I64_t w, v_w;
} succ_t;

#define HQROOT    (0)
#define HQPRNT(x) (((x)-1)>>1)
#define HQLFCH(x) (((x)<<1)+1)

#define HQ_MOVE(hq,nd,i,j)			\
  do {						\
    hq[i].key = hq[j].key;			\
    hq[i].id = hq[j].id;			\
    nd[hq[j].id].offset = i;			\
  } while (0)

static __inline__ void heapify(heap_t *hq, node_t *nd, I64_t id) {
  I64_t i;
  for (i = nd[id].offset; i > HQROOT; i = HQPRNT(i)) {
    if (hq[HQPRNT(i)].key <= nd[id].key) break;
    HQ_MOVE(hq, nd, i, HQPRNT(i));
  }
  hq[i].key = nd[id].key;
  hq[i].id = id;
  nd[id].offset = i;
}

static __inline__ void extractmin(heap_t *hq, node_t *nd, I64_t hqsz) {
  I64_t i = HQROOT, left = HQLFCH(HQROOT);
  heap_t hq_hqsz = hq[hqsz];
  
  /* left and right */
  for ( ; left+1 < hqsz; ) {
    if ( (hq[left].key < hq[left+1].key) &&
	 (hq[left].key < hq_hqsz.key   ) ) {
      HQ_MOVE(hq, nd, i, left);
      i = left;
      left = HQLFCH(i);
    } else if ( !(hq[left].key < hq[left+1].key) &&
		(hq[left+1].key < hq_hqsz.key  ) ) {
      HQ_MOVE(hq, nd, i, left+1);
      i = left+1;
      left = HQLFCH(i);
    } else {
      break;
    }
  }
  
  /* left only */
  if (left+1 == hqsz) {
    if (hq[left].key  < hq_hqsz.key) {
      HQ_MOVE(hq, nd, i, left);
      i = left;
    }
  }
  
  if (i != hqsz) {
    hq[i].key = hq_hqsz.key;
    hq[i].id = hq_hqsz.id;
    nd[hq_hqsz.id].offset = i;
  }
}

#endif /* HEAP_H */
