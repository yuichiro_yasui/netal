#ifndef LOG_H
#define LOG_H

static FILE *logfp = NULL;

#if defined (__cplusplus)
extern "C" {
#endif

void __attribute__ ((constructor)) open_logfile(void) {
  if (!logfp) logfp = fopen("netal_log.txt", "a");
}
void __attribute__  ((destructor)) close_logfile(void) {
  if ( logfp) fclose(logfp);
}

void logprintf(const char* format, ...) {
  va_list args;
  if (stdout) {
    va_start(args, format);
    vfprintf(stdout, format, args);
    va_end(args);
  }
  if (logfp) {
    va_start(args, format);
    vfprintf(logfp, format, args);
    va_end(args);
  }
}

#if defined (__cplusplus)
}
#endif

#endif /* LOG_H */
