#ifndef UTLS_H
#define UTLS_H

#include <stdint.h>

#if defined (__cplusplus)
extern "C" {
#endif
  
  void *xmalloc(size_t sz);
  void verificate(void);

  /* ulibc */
  char *ubasename(const char *string);
  int intlog(int base, unsigned long long int n);
  char *extract_suffix(char *org, char *suffix);
  double *normalize(long n, double *X, double l, double h);
  int64_t *randmized(int64_t n, int64_t *X);
  long long int intsqrt(long long int n);
  double fsqrt(const double x);
  char *set_nsymb(char *s, char symb, int n);
  void print_progressbar(double pcent, char *prefix, char symb, char lf);
  void prange(int64_t sz, int64_t off, int64_t np, int64_t id, int64_t *ls, int64_t *le);
  /* culling */
  void set_culling(int64_t interval);
  int culling(void);
  
  /* timer */
  double tic(void);
  double toc(void);
  double set_timer(double interval, double limit);
  int timelimit(void);
  int interval(void);

#if defined (__cplusplus)
}
#endif

#endif /* UTLS_H */
