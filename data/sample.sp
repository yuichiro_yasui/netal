c file name: sample.sp
c instance name: sample.gr
c created: Tue Aug 16 12:30:33 2011
c
p sp 9 12
c
c centrality (|Vs|=0, BFS, k=0) (|S|=0, random-sampling)
c diameter is 3
c
l Closeness
l Graph
l Betweenness
c
d 1 0.0666667 0.333333 0
d 2 0.0909091 0.333333 2
d 3 0.333333 0.5 2.83333
d 4 0.25 0.5 2.16667
d 5 0.166667 0.5 5
d 6 1 1 2
d 7 1 1 2.16667
d 8 0 0 0
d 9 1 1 0.833333
e 1 2 3
e 1 3 1.83333
e 1 4 3.16667
e 2 5 7
e 3 7 4.83333
e 4 7 2.33333
e 4 9 2.83333
e 5 3 4
e 5 6 5
e 6 8 3
e 7 8 3.16667
e 9 8 1.83333
