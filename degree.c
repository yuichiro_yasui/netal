#include <stdio.h>
#include <math.h>

#include "defs.h"
#include "utls.h"
#include "graph.h"
#include "degree.h"

degdist degree_distributions(SPINT n, SPINT *fwd) {
  SPINT i, od;
  SPINT sum = 0, sum_sq = 0, maxdeg = 0;
  
  for (i = 0; i < n; ++i) {
    od = fwd[i+1] - fwd[i];
    sum_sq += od * od;
    sum += od;
    if (od > maxdeg) maxdeg = od;
  }
  
  degdist dd;
  dd.maxdeg  = maxdeg;
  dd.avg     = (double)sum / n;
  dd.avg_sq  = (double)sum_sq / n;
  dd.var     = dd.avg_sq - (dd.avg * dd.avg);
  dd.std_dev = sqrt(dd.var);
  
  return dd;
}
