#ifndef OPTIONS_H
#define OPTIONS_H

enum {
  USG_KERNEL = 4,
};

enum {
  KERNEL_CENT,
  KERNEL_COMP,
  KERNEL_MSSP,
};

struct usage_t {
  char *name;
  char *args;
  char *detail;
  struct usage_t *sub;
};

extern const char *quickusage;
extern const char *desc;
extern struct usage_t usages[];
extern struct usage_t env_usages[];

struct netal_option_t {
  /* inputs */
  char *graph_file;
  int   graph_type;
  char *graph_desc;
  char *query_file;
  char *dump_file;
  long  kernel_id;
  char  kernel_name[256];
  
  /* library parameter from argv */
  int kernel_flag;
  double accuracy;
  int sequence;
  int weighted;
  int verbose;
  
  /* library parameter from envval */
  long hop_bound;
  int mslc_beta;
};

#if defined (__cplusplus)
extern "C" {
#endif
  struct netal_option_t get_netal_options(int argc, char **argv);
  void finalize_netal_options(struct netal_option_t option);
  int is_known(struct usage_t *usages, const char *s);
  int symbol_to_flag(struct usage_t *usages, char *s);
  char *flag_to_symbol(struct usage_t *usages, int flag);
#if defined (__cplusplus)
}
#endif

#endif /* OPTIONS_H */
