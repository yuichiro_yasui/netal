#ifndef GRAPH_H
#define GRAPH_H

enum {
  GRAPH_DIMACS,
  GRAPH_BINARY,
  GRAPH_SNAP,
  GRAPH_METIS,
};

struct edge_t {
  long long v, w, l;
};

typedef struct {
  int64_t n;
  int64_t m;
  SPINT *forward;
  SPINT *backward;
  SPINT *tail;
  SPINT *head;
  SPINT *length;
  double *X;
  double *Y;
  SPINT *color;
} graph;


#if defined (__cplusplus)
extern "C" {
#endif
  struct edge_t *parse_edges(const char *filename, const int filemode);
  int parse_coords(char *filename, long *n_addr, double **X_addr, double **Y_addr);
  struct edge_t *undirected_filter(struct edge_t *edge);
  graph *construct_graph(struct edge_t *edges, double *X, double *Y);
  graph *copy_graph(graph *G);
  unsigned char *localcopy_graph(graph *G, int nodeid);
  void free_graph(graph *g);
  int dump_edges(const char *dumpname, const int dumpmode, struct edge_t *edges);
  int dump_dimacs_graph(const char *dumpname, graph *G);
  int64_t uniq_nodes(graph *G);
  int uniq_edgelist(struct edge_t *edges,
		    I64_t *n_addr, I64_t *m_addr, I64_t **table_addr);
  int dump_convert_table(const char *dumpname, I64_t n, I64_t m, I64_t *table);
#if defined (__cplusplus)
}
#endif

#endif /* GRAPH_H */
