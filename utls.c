#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ulibc.h>
#include <omp_helpers.h>
#include "utls.h"

void *xmalloc(size_t sz) {
  sz = sz > 0 ? sz : 1;
  void *p = malloc(sz);
  assert(p);
  return memset(p, 0, sz);
}

void verificate(void) {
  char c;
  printf("continue? [Y/n] ");
  while (1) {
    scanf("%c", &c);
    if ( c == 'y' || c == 'n' || c == 'Y' || c == 'N' ) {
      break;
    }
    printf("'y' or 'n' : ");
  }
  if (c == 'n' || c == 'N') exit(0);
}

char *ubasename(const char *string) {
  char *base = strrchr(string, '/');
  if (!base) {
    return (char *)string;
  } else {
    return ++base;
  }
}

int intlog(int base, unsigned long long int n) {
  int log_n=0;
  unsigned long long int crr=1;
  do {
    ++log_n;
    crr *= base;
  } while (crr < n);
  return log_n;
}

/* ------------------------------------------------------------
 * char *extract_suffix(char *org, char *suffix)
 * ------------------------------------------------------------ */
char *extract_suffix(char *org, char *suffix) {
  char *string = NULL;
  if ( !org || !suffix || strlen(org) <= strlen(suffix) ) {
    return org;
  }
  
  assert( string = (char *)malloc(strlen(org)+1) );
  strcpy(string, org);
  
  char *s = strrchr(string, '.');
  if ( !strcmp( s, suffix ) ) {
    *s = '\0';
  }
  return string;
}

double *normalize(long n, double *X, double l, double h) {
  long j;
  double minX, maxX;
  if (X && (h-l) > 0.0 && n > 0) {
    minX = maxX = X[0];
    for (j = 1; j < n; ++j) {
      if (minX > X[j]) minX = X[j];
      if (maxX < X[j]) maxX = X[j];
    }
    for (j = 0; j < n; ++j) {
      X[j] = l + (X[j] - minX) * (h-l) / (maxX-minX);
    }
  }
  return X;
}

int64_t *randmized(int64_t n, int64_t *X) {
  if (!X) return NULL;
  /* 'ulibc' -- ascii --> 1171081059899UL -- sqrt() --> 1082165 */
  const int64_t ULIBC_MAGIC = 1082165UL;
  
  int64_t log2_n = 0;
  while (log2_n*log2_n < n) ++log2_n;
  srand48(log2_n * ULIBC_MAGIC);
    
  for (int64_t i = 0; i < n; i++) {
    const int64_t j = lrand48() % n;
    if (i == j) continue;
    const int64_t k = X[i]; X[i] = X[j]; X[j] = k;
  }
  return X;
}

long long int intsqrt(long long int n) {
  int sqrt_n;
  for (sqrt_n = 0; sqrt_n * sqrt_n < n; ++sqrt_n) ;
  return sqrt_n;
}

double fsqrt(const double x) {
  double xh = 0.5 * x;
  union {
    long long int i;
    double f;
  } xx;
  xx.f = x;
  xx.i = 0x5FE6EB50C7B537AALL - ( *(long long int*)&xx.i >> 1);
  double xr = * (double *)&xx.f;
  xr *= ( 1.5 - ( xh * xr * xr ) );
  xr *= ( 1.5 - ( xh * xr * xr ) );
  xr *= ( 1.5 - ( xh * xr * xr ) );
  return xr * x;
}

char *set_nsymb(char *s, char symb, int n) {
  int i;
  if (s) {
    for (i = 0; i < n; ++i) {
      s[i] = symb;
    }
    s[i] = '\0';
  }
  return s;
}

void print_progressbar(double pcent, char *prefix, char symb, char lf) {
  const int n = 30;
  char buf[128];
  int i, j = (int)(pcent * n);
  for (i = 0; i < j; ++i) {
    buf[i] = symb;
  }
  for (; i < n; ++i) {
    buf[i] = ' ';
  }
  buf[i] = '\0';
  if (prefix) {
    printf("%s\t%s [%3.f%%]%c", prefix, buf, pcent * 100, lf);
  } else {
    printf("\t%s [%3.f%%]%c", buf, pcent * 100, lf);
  }
}

void prange(int64_t sz, int64_t off, int64_t np, int64_t id,
            int64_t *ls, int64_t *le) {
  const int64_t qt = sz / np;
  const int64_t rm = sz % np;
  *ls = qt * (id+0) + (id+0 < rm ? id+0 : rm) + off;
  *le = qt * (id+1) + (id+1 < rm ? id+1 : rm) + off;
}

/* -----------------------------------------------------------------
 * Culling
 * ----------------------------------------------------------------- */
static int64_t _culling_counter = 0;
static int64_t _culling_interval = 20;

void set_culling(int64_t interval) {
  _culling_counter = 0;
  _culling_interval = interval;
}

int culling(void) {
  return (fetch_and_add_int64(&_culling_counter, 1) % _culling_interval == 0);
}

/* -----------------------------------------------------------------
 * Timer
 * ----------------------------------------------------------------- */
struct tictoc_t {
  double start, prev, stop;
};

double tictoc_start(struct tictoc_t *tm) {
  double t = get_msecs();
  if (tm)
    *tm = (struct tictoc_t){ .start = t, .prev = tm->start, .stop = 0 };
  return t;
}

double tictoc_stop(struct tictoc_t *tm) {
  double t = get_msecs();
  if (tm)
    tm->stop = t;
  return t;
}

double tictoc_time(struct tictoc_t *tm) {
  if (tm)
    return tm->stop - tm->start;
  else
    return 0.0;
}

double tictoc_elapsed(struct tictoc_t *tm) {
  if (tm)
    return get_msecs() - tm->start;
  else
    return get_msecs();
}

int tictoc_interval(struct tictoc_t *tm, double delta, double bound) {
  int r = 0;
  double t = get_msecs();
  if (t - tm->prev > delta) {
    r = 1;
    tm->prev = t;
  } 
  if (0.0 < bound && bound < t - tm->start) {
    r = -1;
  }
  return r;
}

static struct tictoc_t _timer;
static double _limit = 0.0;
static double _interval = 0.0;

double tic(void) {
  return tictoc_start(&_timer);
}

double set_timer(double interval, double limit) {
  _interval = interval;
  _limit = limit;
  return tic();
}

double toc(void) {
  return tictoc_elapsed(&_timer);
}

int timelimit(void) {
  /* specified by msec. */
  return tictoc_interval(&_timer, _interval * 1000.0, _limit * 1000.0);
}

int interval(void) {
  /* specified by msec. */
  return tictoc_interval(&_timer, _interval * 1000.0, 0);
}

/* ------------------------- *
 * Local variables:          *
 * c-basic-offset: 2         *
 * indent-tabs-mode: nil     *
 * End:                      *
 * ------------------------- */
