include make.inc

target := netal
objects := options.o utls.o graph.o degree.o stat.o rand_sampling.o sssp.o sssp_k.o component.o cent_kernel.o
objects += mslc_w.o mslc.o dist_kernel.o
# objects += mslc.o distance.o

.PHONY: all clean
all: $(target)

# update_make_headers:
# 	@$(CC) -MM $(addprefix -I, . $(PREFIX)/include) *.c > make.headers
include make.headers

netal : main.o $(objects)
	$(LINK.o) $^ $(LDLIBS) $(OUTPUT_OPTION)
	@echo
	@echo finished successfully: NETAL
	@echo

convert_dimacs : convert_dimacs.o $(objects)
	$(LINK.o) $^ $(LDLIBS) $(OUTPUT_OPTION)

sssp_k.o : CPPFLAGS += -DK_CENT
sssp.o sssp_k.o : sssp.c
	$(COMPILE.c) $< $(OUTPUT_OPTION)

mslc_w.o : CPPFLAGS += -DWEIGHTED
mslc.o mslc_w.o : mslc.c
	$(COMPILE.c) $< $(OUTPUT_OPTION)

clean:
	rm -rf \#* *~ *.o *.dSYM gmon.out a.out $(target) $(objects)
