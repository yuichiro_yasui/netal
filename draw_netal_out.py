#!/usr/bin/env python3
# coding: UTF-8

from sys import argv
from sys import stdin
from time import time
from math import log10
from matplotlib import pyplot
from networkx import Graph
from networkx import get_node_attributes
from networkx import spring_layout
from networkx import draw_networkx_edges
from networkx import draw_networkx_nodes


def main(av):
    print('Usage: cat dimacs.gr dimacs.co netal.out | %s' % argv[0])
    print()
    G = read_graph()
    print('Graph has %d nodes and %d edges' % \
          (G.number_of_nodes(), G.number_of_edges()))
    plot_network(G)


def read_graph(G = Graph(),
               edge_prefix='a', node_prefix='v',
               cent_edge_prefix='e', cent_node_prefix='d'):
    for line in stdin:
        words = list( line.split() )
        if words[0] == edge_prefix:
            v, w, weight = int(words[1]), int(words[2]), float(words[3])
            G.add_edge(v, w, weight=weight)
        if words[0] == node_prefix:
            v, x, y = int(words[1]), float(words[2]), float(words[3])
            G.add_node(v, pos=(x,y))
        if words[0] == cent_edge_prefix:
            v, w, cent = int(words[1]), int(words[2]), [float(words[3])]
            G.add_edge(v, w, cent=cent)
        if words[0] == cent_node_prefix:
            v, cent = int(words[1]), [float(words[2])]
            G.add_node(v, cent=cent)
    return G


def get_bounding_box(G):
    lx = min( [ v['pos'][0] for i, v in G.nodes_iter(data=True) ] )
    ly = min( [ v['pos'][1] for i, v in G.nodes_iter(data=True) ] )
    ux = max( [ v['pos'][0] for i, v in G.nodes_iter(data=True) ] )
    uy = max( [ v['pos'][1] for i, v in G.nodes_iter(data=True) ] )
    return lx, ux, ly, uy


def node_pos(G, pos_key):
    pos = get_node_attributes(G, pos_key)
    if pos == { }:
        return spring_layout(G, weight='weight')
    else:
        return pos


# mycmap = pyplot.cm.rainbow
mycmap = pyplot.cm.Paired

def plot_network(G, figurefile="graph.pdf", ncol=0, ecol=0, eps=0.00001):
    pyplot.figure( figsize=(10,10) )
    lx, ux, ly, uy = get_bounding_box(G)
    pyplot.xlim( [lx-1000, ux+1000] )
    pyplot.ylim( [uy+1000, ly-1000] )

    # def norm(v, eps): return log10( v + eps )
    def norm(v, eps = 0.00001): return v
    
    ecent = []
    for i, j, attr in G.edges_iter(data=True):
        if 'cent' in attr:
            ecent.append( norm( attr['cent'][ecol] ) )
        else:
            ecent.append( log10( eps ) )
    draw_networkx_edges(G, pos=node_pos(G,'pos'),
                        width=0.5, edge_color=ecent, cmap=mycmap)
    ncent = []
    for i, attr in G.nodes_iter(data=True):
        if 'cent' in attr:
            ncent.append( norm( attr['cent'][ncol] ) )
        else:
            ncent.append( log10( eps ) )
    draw_networkx_nodes(G, pos=node_pos(G,'pos'),
                        node_size=15, node_color=ncent, node_shape='o',
                        alpha=0.4, cmap=mycmap, linewidths=0.5)
    pyplot.savefig(figurefile, bbox_inches='tight')


if __name__ == '__main__':
    main(argv[1:])
