#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <sys/stat.h>

#include "ulibc.h"
#include "defs.h"
#include "utls.h"
#include "graph.h"
#include "cent_variant.h"

static char filedesc[256];

/* ------------------------------------------------------------
 *  graph functions
 *    - parse_edges()
 *    - undirected_filter()
 *    - construct_graph()
 *    - free_graph()
 *    - copy_graph()
 * ------------------------------------------------------------ */
static struct edge_t *parse_dimacs_edges(FILE *stream);
static struct edge_t *read_binary_edges(FILE *stream, size_t filesize);
static struct edge_t *parse_snap_edges(FILE *stream);
static struct edge_t *parse_metis_edges(FILE *stream);

struct edge_t *parse_edges(const char *filename, const int filemode) {
  FILE *fp = NULL;
  struct edge_t *edges = NULL;
  struct stat statbuf;
  if (filename) {
    fp = fopen(filename, "r");
    stat(filename, &statbuf);
    strcpy(filedesc, ubasename(filename));
  }
  if (fp) {
    switch (filemode) {
    case GRAPH_DIMACS : edges = parse_dimacs_edges(fp); break;
    case GRAPH_BINARY : edges = read_binary_edges(fp, statbuf.st_size); break;
    case GRAPH_SNAP   : edges = parse_snap_edges(fp); break;
    case GRAPH_METIS  : edges = parse_metis_edges(fp); break;
    default           : edges = NULL; break;
    }
    fclose(fp);
  }
  return edges;
}

static int parse_dimacs_coords(FILE *stream,
                               long *n_addr, double **X_addr, double **Y_addr);

int parse_coords(char *filename, long *n_addr, double **X_addr, double **Y_addr) {
  FILE *fp = NULL;
  if (filename) {
    fp = fopen(filename, "r");
    strcpy(filedesc, ubasename(filename));
  }
  int ret = parse_dimacs_coords(fp, n_addr, X_addr, Y_addr);
  fclose(fp);
  return ret;
}

int edgecmp(const void *a, const void *b) {
  struct edge_t _a = *(struct edge_t *)a;
  struct edge_t _b = *(struct edge_t *)b;
  if (_a.v != _b.v) {
    return (_a.v < _b.v) ? -1 : 1;
  } else {
    return (_a.w == _b.w) ? 0 : ((_a.w < _b.w) ? -1 : 1);
  }
}

struct edge_t *undirected_filter(struct edge_t *edge) {
  SPINT i, j, old_m, uniq_m, new_m;
  for (i = 0; edge[i].v >= 0; ++i) {
    if ( edge[i].v > edge[i].w ) {
      struct edge_t t = edge[i];
      edge[i].v = t.w;
      edge[i].w = t.v;
      edge[i].l = t.l;
    }
  }
  j = uniq(edge, i, sizeof(struct edge_t), qsort, edgecmp);
  old_m  = i;
  uniq_m = j;
  new_m  = j*2;
  if (old_m != new_m) {
    printf("expand edge array (%lld -> %lld)\n", old_m, new_m);
    assert( edge = (struct edge_t *)realloc(edge, sizeof(struct edge_t) * (new_m+1)) );
    for (i = 0; i < uniq_m; ++i, ++j) {
      assert( j < new_m );
      edge[j].v = edge[i].w;
      edge[j].w = edge[i].v;
      edge[j].l = edge[i].l;
    }
    edge[j].v = -1;
    edge[j].w = -1;
    edge[j].l = -1;
    assert( j == new_m );
  }
  return edge;
}

static void detect_graph_size(struct edge_t *edge, int64_t *n_addr, int64_t *m_addr);
static SPINT *prefix_sum(int is_forward, SPINT n, SPINT m, struct edge_t *edge);
static SPINT *adjacency_list_pointer(SPINT n, SPINT m, struct edge_t *edge,
                                     SPINT *head, SPINT *length);
graph *construct_graph(struct edge_t *edges, double *X, double *Y) {
  graph *g = NULL;
  assert( g = (graph *)calloc(1, sizeof(graph)) );
  
  detect_graph_size(edges, &(g->n), &(g->m));
  g->head     = (SPINT *)xmalloc(sizeof(SPINT) * (g->m+1));
  g->length   = (SPINT *)xmalloc(sizeof(SPINT) * (g->m+1));
  g->forward  = adjacency_list_pointer(g->n, g->m, edges, g->head, g->length);
  g->backward = prefix_sum(0, g->n, g->m, edges);
  if (X) {
    size_t sz = sizeof(double) * (g->n);
    g->X = memcpy(malloc(sz), X, sz);
  }
  if (Y) {
    size_t sz = sizeof(double) * (g->n);
    g->Y = memcpy(malloc(sz), Y, sz);
  }
  g->color = NULL;
  
  return g;
}

void free_graph(graph *g) {
  if ( g ) {
    free(g->forward);
    free(g->backward);
    free(g->tail);
    free(g->head);
    free(g->length);
    free(g->X);
    free(g->Y);
    free(g->color);
    free(g);
  }
}

graph *copy_graph(graph *G) {
  graph *LG = (graph *)xmalloc(sizeof(graph));
  assert(LG);
  LG->n = G->n;
  LG->m = G->m;
  
  size_t sz;
  if (G->forward) {
    sz = sizeof(SPINT) * (G->n+1);
    LG->forward = memcpy(xmalloc(sz), G->forward, sz);
  }
  if (G->backward) {
    sz = sizeof(SPINT) * (G->n+1);
    LG->backward = memcpy(xmalloc(sz), G->backward, sz);
  }
  if (G->head) {
    sz = sizeof(SPINT) * (G->m+1);
    LG->head = memcpy(xmalloc(sz), G->head, sz);
  }
  if (G->length) {
    sz = sizeof(SPINT) * (G->m+1);
    LG->length = memcpy(xmalloc(sz), G->length, sz);
  }
  if (G->X) {
    sz = sizeof(double) * (G->n);
    LG->X = memcpy(xmalloc(sz), G->X, sz);
  }
  if (G->Y) {
    sz = sizeof(double) * (G->n);
    LG->Y = memcpy(xmalloc(sz), G->Y, sz);
  }
  if (G->color) {
    sz = sizeof(I64_t) * (G->n);
    LG->color = memcpy(xmalloc(sz), G->color, sz);
  }
  return LG;
}

unsigned char *localcopy_graph(graph *G, int nodeid) {
  const size_t spacing = 64;
  size_t sz = 0;
  sz += sizeof(graph) + spacing;
  sz += (G->forward  != NULL) * sizeof(SPINT)  * (G->n+1) + spacing;
  sz += (G->backward != NULL) * sizeof(SPINT)  * (G->n+1) + spacing;
  sz += (G->head     != NULL) * sizeof(SPINT)  * (G->m+1) + spacing;
  sz += (G->length   != NULL) * sizeof(SPINT)  * (G->m+1) + spacing;
  sz += (G->X        != NULL) * sizeof(double) * (G->n  ) + spacing;
  sz += (G->Y        != NULL) * sizeof(double) * (G->n  ) + spacing;
  sz += (G->color    != NULL) * sizeof(I64_t)  * (G->n  ) + spacing;
  
  unsigned char *pool = NUMA_touched_malloc(sz, nodeid);
  I64_t off = 0;
  graph *LG = (graph *)&pool[off];
  LG->n = G->n;
  LG->m = G->m;
  off += sizeof(graph) + spacing;
    
  if (G->forward) {
    sz = sizeof(SPINT) * (G->n+1);
    LG->forward = memcpy(&pool[off], G->forward, sz);
    off += sz + spacing;
  }
  
  if (G->backward) {
    sz = sizeof(SPINT) * (G->n+1);
    LG->backward = memcpy(&pool[off], G->backward, sz);
    off += sz + spacing;
  }
  
  if (G->head) {
    sz = sizeof(SPINT) * (G->m+1);
    LG->head = memcpy(&pool[off], G->head, sz);
    off += sz + spacing;
  }

  if (G->length) {
    sz = sizeof(SPINT) * (G->m+1);
    LG->length = memcpy(&pool[off], G->length, sz);
    off += sz + spacing;
  }
  
  if (G->X) {
    sz = sizeof(double) * (G->n);
    LG->X = memcpy(&pool[off], G->X, sz);
    off += sz + spacing;
  }
  
  if (G->Y) {
    sz = sizeof(double) * (G->n);
    LG->Y = memcpy(&pool[off], G->Y, sz);
    off += sz + spacing;
  }
  
  if (G->color) {
    sz = sizeof(I64_t) * (G->n);
    LG->color = memcpy(&pool[off], G->color, sz);
    off += sz + spacing;
  }
  
  return pool;
}




/* ------------------------------------------------------------
 * dumper
 *   dump_dimacs_graph()
 *   dump_edges()
 * ------------------------------------------------------------ */
int dump_dimacs_graph(const char *dumpname, graph *G) {
  FILE *fp = NULL;
  I64_t i, j;
  if (dumpname) {
    fp = fopen(dumpname, "w");
  }
  if (fp) {
    fprintf(fp, "p sp %" PRId64 " %" PRId64 "\n", G->n, G->m);
    for (i = 0; i < G->n; ++i) {
      for (j = G->forward[i]; j < G->forward[i+1]; ++j) {
        fprintf(fp, "a %lld %lld %lld\n", i+1, G->head[j]+1, G->length[j]);
      }
    }
    fclose(fp);
    return 0;
  } else {
    return 1;
  }
}

static int dump_dimacs_edges(FILE *stream, struct edge_t *edges);
static int dump_binary_edges(FILE *stream, struct edge_t *edges);

int dump_edges(const char *dumpname, const int dumpmode, struct edge_t *edges) {
  FILE *fp = NULL;
  if (dumpname) {
    fp = fopen(dumpname, "w");
  }
  if (fp) {
    switch (dumpmode) {
    case GRAPH_DIMACS: dump_dimacs_edges(fp, edges); break;
    case GRAPH_BINARY: dump_binary_edges(fp, edges); break;
    default: ;
    }
    fclose(fp);
    return 0;
  } else {
    return 1;
  }
}

static int dump_dimacs_edges(FILE *stream, struct edge_t *edges) {
  I64_t j, n, m;
  if (stream && edges) {
    n = 0;
    for (j = 0; edges[j].v >= 0; ++j) {
      if (n < edges[j].v) n = edges[j].v;
      if (n < edges[j].w) n = edges[j].w;
    }
    n = n+1;
    m = j;
    fprintf(stream, "p sp %lld %lld\n", (I64_t)n, (I64_t)m);
    for (j = 0; j < m; ++j) {
      fprintf(stream, "a %lld %lld %lld\n", edges[j].v+1, edges[j].w+1, edges[j].l);
    }
    return 0;
  } else {
    return 1;
  }
}

static int dump_binary_edges(FILE *stream, struct edge_t *edges) {
  size_t m;
  if (stream && edges) {
    for (m = 0; edges[m].v >= 0; ++m) ;
    assert( fwrite( edges, sizeof(struct edge_t), m, stream ) == m );
    return 0;
  } else {
    return 1;
  }
}








/* ############################################################
 * private functions
 * ############################################################ */


/* ------------------------------------------------------------
 * DIMACS edge-parser
 * ------------------------------------------------------------ */
static inline struct edge_t read_dimacs_edge_line(char *line) {
  struct edge_t e;
  e.v = strtoll(line, &line, 10) - 1;
  e.w = strtoll(line, &line, 10) - 1;
  e.l = strtoll(line, &line, 10);
  if (e.l == 0) e.l = 1;
  return e;
}
static inline long read_dimacs_line(char *line, long *n_addr, long *m_addr) {
  char ide[128];
  long pn=0, pm=0;
  if ( sscanf(&line[1], "%s %ld %ld", ide, &pn, &pm) == 4 ) {
    if ( strcmp(ide, "sp") ) {
      return 0;
    }
  }
  if (n_addr) *n_addr = pn;
  if (m_addr) *m_addr = pm;
  return pm;
}
static struct edge_t *parse_dimacs_edges(FILE *stream) {
  char line[LINEMAX+1], msg[LINEMAX+1];
  long n=0, m=0, ln, el=0;
  struct edge_t *edges=NULL;
  set_timer(0.2, 0.0);
  if (stream) {
    for (ln = 1; fgets(line, LINEMAX, stream); ++ln) {
      if (line[0] == 'a') {
        edges[el++] = read_dimacs_edge_line(&line[1]);
 	if ( el == m || interval() ) {
	  print_progressbar((double)el/m, msg, '#', (el == m) ? '\n' : '\r');
	}
      } else if (line[0] == 'p') {
        read_dimacs_line(&line[1], &n, &m);
        if (m != 0) {
          printf("[p] %s", line);
          assert( edges = malloc(sizeof(struct edge_t) * (m+1)) );
          sprintf(msg, "\t%s (n=%ld, m=%ld)", filedesc, n, m);
        } else {
          printf("[error] %s", line);
        }
      } else {
        printf("[c] %s", line);
      }
    }
    assert(el == m);
    edges[m].v = edges[m].w = edges[m].l = -1;
  }
  return edges;
}


/* ------------------------------------------------------------
 * binary edge-parser
 * ------------------------------------------------------------ */
static struct edge_t *read_binary_edges(FILE *stream, size_t filesize) {
  struct edge_t *edges = NULL;
  if (stream) {
    I64_t m = filesize / sizeof(struct edge_t);
    assert( edges = (struct edge_t *)malloc(sizeof(struct edge_t) * (m+1)) );
    assert( fread(edges, sizeof(struct edge_t), m, stream) == (size_t)m);
    edges[m].v = edges[m].w = edges[m].l = -1;
  }
  return edges;
}


/* ------------------------------------------------------------
 * SNAP edge-parser
 * ------------------------------------------------------------ */
static inline struct edge_t read_snap_edge_line(char *line) {
  struct edge_t e;
  e.v = strtoll(line, &line, 10);
  e.w = strtoll(line, &line, 10);
  e.l = strtoll(line, &line, 10);
  if (e.l == 0) e.l = 1;
  return e;
}

static struct edge_t *parse_snap_edges(FILE *fp) {
  char line[LINEMAX+1];
  struct edge_t *edges = NULL;
  long m = 0, cap = 0, block = 1<<20;
  if (fp) {
    while ( fgets(line, LINEMAX, fp) ) {
      if (line[0] == '#') {
        printf("%s", line);
      } else {
        if ( m+1 > cap ) {
          cap += block;
          assert( edges = (struct edge_t *)realloc(edges, sizeof(struct edge_t)*cap) );
        }
        edges[m++] = read_snap_edge_line(line);
      }
    }
  }
  if ( m+1 > cap ) {
    cap += block;
    assert( edges = (struct edge_t *)realloc(edges, sizeof(struct edge_t) * cap) );
  }
  edges[m].v = edges[m].w = edges[m].l = -1;
  return edges;
}



/* ------------------------------------------------------------
 * METIS constructer
 * ------------------------------------------------------------ */
static void skip_line(FILE *fp) {
  int c = 0;
  /* ungetc(c, fp); */
  while ( (c = getc(fp)) != EOF ) {
    putc(c, stdout);
    if (c == '\n') break;
  }
}

static long long strtoll_stream(FILE *fp, char *endc) {
  char buf[128];
  int c, at = 0;
  while ( (c = getc(fp)) != EOF ) {
    if ( isdigit(c) ) {
      buf[at++] = c;
    } else {
      if (at == 0) continue;
      buf[at] = '\0';
      if (endc) *endc = c;
      /* printf("[%s]", buf); */
      return strtoll(buf, NULL, 10);
    }
  }
  if (endc) *endc = c;
  return 0;
}

enum { NO_WEIGHTS          =  00,
       EDGE_WEIGHTS        =  01,
       VERTEX_WEIGHTS      =  10,
       EDGE_VERTEX_WEIGHTS =  11,
       MULTIPLE_EDGES      = 100};

static struct edge_t *parse_metis_edges(FILE *stream) {
  char c;
  int FMT = 0;
  long n=0, m=0, v, el = 0;
  struct edge_t *edges = NULL;
  
  /* parse 'n' and 'm' */
  while ( (c = getc(stream)) != EOF ) {
    if (c == '%') {
      ungetc(c, stream);
      printf("[c] ");
      skip_line(stream);
    } else {
      ungetc(c, stream);
      fscanf(stream, "%ld %ld %d", &n, &m, &FMT);
      m *= 2;
      skip_line(stream);
      break;
    } 
  }
  switch (FMT) {
  case MULTIPLE_EDGES:
  case NO_WEIGHTS:
    printf("[c] unweighted-METIS graph (n=%ld, m=%ld, FMT=%d)\n", n, m, FMT); break;
  case EDGE_WEIGHTS:
    printf("[c] weighted-METIS graph (n=%ld, m=%ld, FMT=%d)\n", n, m, FMT); break;
  case VERTEX_WEIGHTS:
  case EDGE_VERTEX_WEIGHTS:
  default :
    fprintf(stderr, "ERROR : not support metis format\n"); exit(1);
  }
  assert( edges = malloc(sizeof(struct edge_t) * (m+1)) );
  
  /* parse edge lines */
  for (v = 1; v <= n; ++v) {
    if ( (c = getc(stream)) == EOF ) break;
    if ( c == '%' ) {
      --v;
      ungetc(c, stream);
      printf("[c] ");
      skip_line(stream);
    } else if ( c == '\n' ) {
      /* do nothing */
    } else {
      ungetc(c, stream);
      do {
        edges[el].v = v-1;
        edges[el].w = strtoll_stream(stream, &c)-1;
        if (FMT == NO_WEIGHTS) {
          edges[el].l = 1;
        } else if (FMT == EDGE_WEIGHTS) {
          edges[el].l = strtoll_stream(stream, &c);
        }
        ++el;
      } while ( c != '\n' && c != EOF );
    }
  }
  assert(el == m);
  edges[el].v = edges[el].w = edges[el].l = -1;
  return edges;
}


/* ------------------------------------------------------------
 * DIMACS coordinates-parser
 * ------------------------------------------------------------ */
struct point_t {
  I64_t i;
  double x;
  double y;
};
static inline struct point_t read_dimacs_xy_line(char *line) {
  struct point_t p;
  p.i = strtoll(line, &line, 10) - 1;
  p.x = strtod (line, &line);
  p.y = strtod (line, &line);
  return p;
}
static inline long read_dimacs_co_line(char *line) {
  char ide[128], pro[128], fmt[128];
  long pn=0;
  if ( sscanf(&line[1], "%s %s %s %ld", ide, pro, fmt, &pn) == 4 ) {
    if ( strcmp(ide, "aux") || strcmp(pro, "sp") || strcmp(fmt, "co") ) {
      return 0;
    }
  }
  return pn;
}
static int parse_dimacs_coords(FILE *stream,
                               long *n_addr, double **X_addr, double **Y_addr) {
  char line[LINEMAX+1], msg[LINEMAX+1];
  long nr=0, n=0;
  double *X=NULL, *Y=NULL;
  struct point_t p;
  set_timer(0.2, 0.0);
  if (stream) {
    while ( fgets(line, LINEMAX, stream) ) {
      if (line[0] == 'v') {
	p = read_dimacs_xy_line(&line[1]);
	X[p.i] = p.x;
	Y[p.i] = p.y;
	if ( ++nr == n || interval() ) {
	  print_progressbar((double)nr/n, msg, '#', (nr == n) ? '\n' : '\r');
	}
      } else if (line[0] == 'p') {
        n = read_dimacs_co_line(&line[1]);
        sprintf(msg, "\t%s (n=%ld)\t\t", filedesc, n);
	X = (double *)calloc(n, sizeof(double));
	Y = (double *)calloc(n, sizeof(double));
        assert(X && Y);
      }
    }
    assert(nr == n);
  }
  if (n_addr) *n_addr = n;
  if (X_addr) *X_addr = normalize(n, X, 0.0, COORD_NORMALIZE(n));
  if (Y_addr) *Y_addr = normalize(n, Y, 0.0, COORD_NORMALIZE(n));
  
  return 0;
}




/* ------------------------------------------------------------
 * adjacency list representation
 * ------------------------------------------------------------ */
static void detect_graph_size(struct edge_t *edges, int64_t *n_addr, int64_t *m_addr) {
  SPINT m, max_n = 0, min_n = 0;
  
  for (m = 0; edges[m].v >= 0; ++m) {
    if (min_n > edges[m].v) min_n = edges[m].v;
    if (min_n > edges[m].w) min_n = edges[m].w;
    if (max_n < edges[m].v) max_n = edges[m].v;
    if (max_n < edges[m].w) max_n = edges[m].w;
  }
  
  if (min_n < 0) {
    fprintf(stderr, "[error] min(node_id) is %lld\n", (I64_t)min_n);
    exit(1);
  }
  
  if (n_addr) *n_addr = max_n+1;
  if (m_addr) *m_addr = m;
}


static SPINT *prefix_sum(int is_forward, SPINT n, SPINT m, struct edge_t *edge) {
  SPINT i, j, *start;
  start = (SPINT *)calloc(n+1, sizeof(SPINT));
  assert( start );
  
  if (is_forward) {
    for (j = 0; j < m; ++j) {
      start[ edge[j].v + 1 ]++;
    }
  } else {
    for (j = 0; j < m; ++j) {
      start[ edge[j].w + 1 ]++;
    }
  }
  
  for (i = 1; i <= n; ++i) {
    start[i] += start[i-1];
  }
  
  return start;
}


static SPINT *adjacency_list_pointer(SPINT n, SPINT m,
                                     struct edge_t *edge,
                                     SPINT *head, SPINT *length) {
  assert( n > 1 && m > 1 );
  assert( edge && head && length );
  
  SPINT i, j, k, *start;
  start = prefix_sum(1, n, m, edge);
  
  for (j = 0; j < m; ++j) {
    k = start[ edge[j].v ]++;
    head[k] = edge[j].w;
    length[k] = edge[j].l;
  }
  for (i = n; i > 0; --i) {
    start[i] = start[i-1];
  }
  start[0] = 0;
  
  return start;
}




/* ------------------------------------------------------------
 * uniq_edgelist
 * uniq_nodes
 * ------------------------------------------------------------ */
static int I64cmp(const void *a, const void *b) {
  const I64_t _a = *(const I64_t *)a;
  const I64_t _b = *(const I64_t *)b;
  if (_a  > _b) return  1;
  if (_b  > _a) return -1;
  if (_a == _b) return  0;
  return 0;
}


int uniq_edgelist(struct edge_t *edges,
                  I64_t *n_addr, I64_t *m_addr, I64_t **table_addr) {
  I64_t i, j, n, m;
  I64_t uniq_n, *id_set=NULL, *id_table=NULL;
  
  /* counting #edges */
  for (m = 0; edges[m].v >= 0; ++m) { }
  
  /* create id_list */
  assert( id_set = (I64_t *)malloc(sizeof(I64_t) * (m*2)) );
  for (i = j = 0; j < m; i+=2, ++j) {
    id_set[i+0] = edges[j].v;
    id_set[i+1] = edges[j].w;
  }
  uniq_n = uniq(id_set, m*2, sizeof(I64_t), qsort, I64cmp);
  n = id_set[uniq_n-1]+1;
  printf("n is %lld, m is %lld\n", n, m);
  printf("uniq_n is %lld\n", uniq_n);
  
  /* convert table */
  assert( id_table = (I64_t *)malloc(sizeof(I64_t) * (n)) );
  memset(id_table, -1, sizeof(I64_t) * (n));
  for (j = 0; j < uniq_n; ++j) {
    id_table[ id_set[j] ] = j;
  }
  free(id_set);
  
  /* convert serial-id */
  for (j = 0; j < m; ++j) {
    /* assert(id_table[edge[j].v] >= 0); */
    /* assert(id_table[edge[j].w] >= 0); */
    edges[j].v = id_table[ edges[j].v ];
    edges[j].w = id_table[ edges[j].w ];
  }
  edges[m].v = edges[m].w = edges[m].l = -1;
    
  if (n_addr) *n_addr = uniq_n+1;
  if (m_addr) *m_addr = m;
  if (table_addr) {
    *table_addr = id_table;
  } else {
    free(id_table);
  }
  
  return 0;
}


int dump_convert_table(const char *dumpname, I64_t n, I64_t m, I64_t *table) {
  FILE *fp = NULL;
  if (dumpname) {
    fp = fopen(dumpname, "w");
  }
  if (fp) {
    I64_t j;
    fprintf(fp, "p sp %lld %lld\n", n, m);
    fprintf(fp, "c new_node_id original_node_id\n");
    for (j = 0; j < n; ++j) {
      if (table[j] >= 0) {
        fprintf(fp, "d %lld %lld\n", j+1, table[j]+1);
      }
    }
    fclose(fp);
    return 0;
  }
  return 1;
}


int64_t uniq_nodes(graph *G) {
  int64_t v, j, uniq_sz, *id_set, id_set_sz = 0;
  assert( id_set = (int64_t *)malloc(sizeof(int64_t) * (G->m*2)) );
  for (v = 0; v < G->n; ++v) {
    if (G->forward[v+1] - G->forward[v] > 0) {
      id_set[id_set_sz++] = v;
    }
    for (j = G->forward[v]; j < G->forward[v+1]; ++j) {
      id_set[id_set_sz++] = G->head[j];
    }
  }
  uniq_sz = uniq(id_set, id_set_sz, sizeof(int64_t), qsort, I64cmp);
  free(id_set);
  /* I64_t max_id = id_set[uniq_sz-1]+1; */
  return uniq_sz;
}




/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 */
