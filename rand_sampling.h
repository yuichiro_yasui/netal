#ifndef RAND_SAMPLING_H
#define RAND_SAMPLING_H

struct blocked_vertices_t {
  /* --------------------------------------------------
   * sources[nv] = { V[0] | V[1] | ... | V[nblocks-1] }
   * |V[0]| = |V[1]| = ... = |V[nblocks-1]| = block_size
   * --------------------------------------------------
   */
  int64_t nv;
  int64_t block_size;
  int64_t nblocks;
  int64_t *start;
  int64_t *sources;
};

#if defined (__cplusplus)
extern "C" {
#endif
  int64_t *make_roots(int isseq, graph *G, int64_t *nv_addr);
  struct blocked_vertices_t make_blocked_vertices(graph *G, int64_t block_size);

#if defined (__cplusplus)
}
#endif

#endif /* RAND_SAMPLING_H */
