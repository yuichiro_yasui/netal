#include <stdio.h>
#include <stdlib.h>

#include "defs.h"
#include "utls.h"
#include "graph.h"
#include "heap.h"
#include "mslc.h"

#ifdef WEIGHTED
#define FUNCNAME MSSP_mslc_weighted
#else
#define FUNCNAME MSSP_mslc
#endif

result_t FUNCNAME(int beta, SPINT *fd, SPINT *hd, SPINT *ln,
                  SPINT ns, int64_t *s, node_t *nd, SPINT *ds, heap_t *hq) {
#ifndef WEIGHTED
  (void)ln;
#endif
  SPINT i, j, v, w, l, hqsz;
  SPINT ites = 0, scanned = 0;
  SPINT w_j, *ds_v_j, *ds_w_j;
  SPINT key_w;
  
  /* initialize */
  hqsz = 0;
  v = s[0]; /* global source */

  for (j = 0; j < ns; ++j) {
    w         = s[j];
    nd[w].key = 0;
    w_j       = w * beta + j;
    ds[w_j]   = 0;
    if (j == 0) {
      nd[v].offset = -1;
    } else {
      nd[w].offset = hqsz++;
      heapify(hq, nd, w);
    }
  }
  
  do {
    ++ites;
    scanned += fd[v+1] - fd[v];
    
    /* for each (v,w) in E outgoing form v */
    for (i = fd[v]; i < fd[v+1]; ++i) {
      w = hd[i];
#ifdef WEIGHTED
      l = ln[i];
#else
      l = 1;
#endif

      key_w = SPINT_MAX;
      
      ds_v_j = &ds[ v * beta ];
      ds_w_j = &ds[ w * beta ];
      j = ns;
      while (j > 0) {
	if ( ((U64_t)*ds_v_j + l < (U64_t)*ds_w_j) && 0 <= *ds_v_j ) {
	  *ds_w_j = *ds_v_j + l;
          if (*ds_w_j < key_w) key_w = *ds_w_j;
	}
        ++ds_v_j;
        ++ds_w_j;
        --j;
      }

      if (key_w < SPINT_MAX) {
	nd[w].key = key_w;
	if (nd[w].offset < 0) {
	  nd[w].offset = hqsz++;
	}
	heapify(hq, nd, w);
      }
    }
    
    /* extractmin */
    if (--hqsz < HQROOT) {
      v = -1;
    } else {
      v = hq[HQROOT].id;
      nd[ hq[HQROOT].id ].offset = -1;
      extractmin(hq, nd, hqsz);
    }
  } while (v >= 0);
  
  result_t r;
  r.trav_nodes = ites;
  r.trav_edges = scanned;
  r.CC = 0.0;
  r.GC = 0.0;
  return r;
}


/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 */
