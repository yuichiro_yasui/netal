### customize binary
# data type
BUILD_ILP64 = Yes

# turn on fall tolerance
BUILD_FALL_TOLERANCE = Yes

### customize options
CC          := gcc
CFLAGS      := -g3
PREFIX      := $(HOME)/local
CPPFLAGS    := $(addprefix -I, . $(PREFIX)/include)
LDFLAGS     := $(addprefix -L, . $(PREFIX)/lib)
TARGET_ARCH :=
LDLIBS      := -lm -lz -lulibc

OS = $(shell uname -s)
ifeq ($(OS), SunOS)
CFLAGS   += -O3 -m64 -xopenmp -xc99 -D_XOPEN_SOURCE=600
LDLIBS   += -llgrp -lkstat -lpicl -xopenmp
LDFLAGS  += $(addprefix -R, . $(PREFIX)/lib) -O3 -m64
else ifeq ($(OS), AIX)
CFLAGS   += -qsmp=omp
LDLIBS   += -qsmp=omp
else ifeq ($(OS), Linux)
CFLAGS   += -O2 -fopenmp -Wall -Wextra -std=c99 -D_GNU_SOURCE
LDFLAGS  += -Wl,-no-as-needed -Wl,-R$(PREFIX)/lib -Wl,-rpath-link $(PREFIX)/lib
LDLIBS   += -fopenmp -lpthread
else ifeq ($(OS), Hwloc)
CFLAGS   += -O2 -fopenmp -Wall -Wextra -std=c99 -D_GNU_SOURCE
LDLIBS   += -fopenmp -lpthread
else ifeq ($(OS), Darwin)
CFLAGS   += -O2 -fopenmp -Wall -Wextra -std=c99 -D_GNU_SOURCE
LDFLAGS  += -Wl,-rpath,$(PREFIX)/lib
LDLIBS   += -fopenmp -lpthread
else
CFLAGS   += -O2 -fopenmp -Wall -Wextra -std=c99 -D_GNU_SOURCE
LDLIBS   += -fopenmp -lpthread
endif

# ------------------------------------------------------------
# customize
# ------------------------------------------------------------
ifeq ($(BUILD_ILP64), Yes)
    CPPFLAGS += -DNETAL_ILP64
endif

ifeq ($(BUILD_FALL_TOLERANCE), Yes)
    CPPFLAGS += -DFALL_TOLERANCE
endif



# ------------------------------------------------------------

### Local variables:
### mode: makefile
### coding: utf-8-unix
### indent-tabs-mode: nil
### End:
