#ifndef SSSP_H
#define SSSP_H

#if defined (__cplusplus)
extern "C" {
#endif
  result_t SP_SSSP_dijkstra(SPINT *fd, SPINT *hd, SPINT *ln,
			    SPINT s, SPINT t,
			    node_t *nd, heap_t *hq, SPINT *succ);
  result_t SP_kSSSP_dijkstra(SPINT *fd, SPINT *hd, SPINT *ln,
			     SPINT s, SPINT t,
			     node_t *nd, heap_t *hq, SPINT *succ,
			     SPINT k, SPINT *hop);
  
  result_t MP_SSSP_dijkstra(SPINT *fd, SPINT *hd, SPINT *ln,
			    SPINT s, node_t *nd, heap_t *hq,
			    succ_t *list, SPINT *count,
			    SPINT *queue, double *sigma);
  result_t MP_kSSSP_dijkstra(SPINT *fd, SPINT *hd, SPINT *ln,
			     SPINT s, node_t *nd, heap_t *hq,
			     succ_t *list, SPINT *count,
			     SPINT *queue, double *sigma,
			     SPINT k, SPINT *hop);
  
  result_t MP_BFS(SPINT *fd, SPINT *hd, SPINT s, 
		  SPINT *ds, succ_t *list, SPINT *count,
		  SPINT *queue, double *sigma);
  result_t MP_kBFS(SPINT *fd, SPINT *hd, SPINT s, 
		   SPINT *ds, succ_t *list, SPINT *count,
		   SPINT *queue, double *sigma, SPINT k);
#if defined (__cplusplus)
}
#endif

#endif /* SSSP_H */

