#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "defs.h"
#include "utls.h"
#include "stat.h"

#define NSTAT 9
static void statistics(double *out, double *data, I64_t n);

void output_results(const I64_t ites,
		    const double *sssp_times, const I64_t *trav_edges) {
  double *datas = (double *)xmalloc(ites  * sizeof(double));
  double stats[3][9];
  I64_t i, j, k;
  char *name[9] = {
    "min", "1/4", "median", "3/4" , "max",
    "mean", "stddev", "harmonic-mean", "harmonic-stddev"
  };
  
  for (k = 0; k < ites; ++k) { datas[k] = sssp_times[k]; }
  statistics(stats[0], datas, ites);
  for (k = 0; k < ites; ++k) { datas[k] = (double)trav_edges[k]; }
  statistics(stats[1], datas, ites);
  for (k = 0; k < ites; ++k) { datas[k] = (double)trav_edges[k] / sssp_times[k] / 1e3; }
  statistics(stats[2], datas, ites);
  
  printf("----------------------------------------------------------------------\n");
  printf("%-15s  %15s  %15s  %15s\n", "statistics", "time[ms]", "scanned", "MTEPS");
  for (i = 0; i < 9; ++i) {
    printf("%-15s  ", name[i]);
    for (j = 0; j < 3; ++j) {printf("%15f  ", stats[j][i]); }
    printf("\n");
  }
  printf("----------------------------------------------------------------------\n");
  free(datas);
}

static int f64cmp(const void *a, const void *b) {
  const double _a = *(const double*)a;
  const double _b = *(const double*)b;
  if (_a  > _b) return  1;
  if (_b  > _a) return -1;
  if (_a == _b) return  0;
  return 0;
}

static void statistics(double *stats, double *datas, I64_t n) {
  long double s, mean;
  double t;
  int k;
  
  qsort(datas, n, sizeof(double), f64cmp);

  /* 0: min */
  stats[0] = datas[0];
  
  /* 1: firstquartile */
  t = (n+1) / 4.0;
  k = (int) t;
  if (t == k) {
    stats[1] = datas[k];
  } else {
    stats[1] = 3*(datas[k]/4.0) + datas[k+1]/4.0;
  }

  /* 2: median */
  t = (n+1) / 2.0;
  k = (int) t;
  if (t == k)
    stats[2] = datas[k];
  else
    stats[2] = datas[k]/2.0 + datas[k+1]/2.0;
  
  /* 3: thirdquartile */
  t = 3*((n+1) / 4.0);
  k = (int) t;
  if (t == k)
    stats[3] = datas[k];
  else
    stats[3] = datas[k]/4.0 + 3*(datas[k+1]/4.0);
  
  /* 4: max */
  stats[4] = datas[n-1];
  
  /* 5: mean */
  s = datas[n-1];
  for (k = n-1; k > 0; --k)
    s += datas[k-1];
  mean = s/n;
  stats[5] = mean;
  
  /* 6:stddev */
  s = datas[n-1] - mean;
  s *= s;
  for (k = n-1; k > 0; --k) {
    long double tmp = datas[k-1] - mean;
    s += tmp * tmp;
  }
  stats[6] = sqrt (s/(n-1));
  
  /* 7: harmonic_mean */
  s = datas[0] ? 1.0L/datas[0] : 0;
  for (k = 1; k < n; ++k) {
    s += (datas[k]? 1.0L/datas[k] : 0);
  }
  stats[7] = n/s;
  mean = s/n;

  /*
    Nilan Norris, The Standard Errors of the Geometric and Harmonic
    Means and Their Application to Index Numbers, 1940.
    http://www.jstor.org/stable/2235723
  */
  /* 8: harmonic_stddev */
  s = (datas[0]? 1.0L/datas[0] : 0) - mean;
  s *= s;
  for (k = 1; k < n; ++k) {
    long double tmp = (datas[k]? 1.0L/datas[k] : 0) - mean;
    s += tmp * tmp;
  }
  s = (sqrt (s)/(n-1)) * stats[7] * stats[7];
  stats[8] = s;
}
