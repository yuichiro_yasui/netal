#include <stdio.h>
#include <math.h>
#include <time.h>

#include <ulibc.h>
#include <omp_helpers.h>

#include "defs.h"
#include "utls.h"
#include "graph.h"
#include "cent_variant.h"
#include "dist_kernel.h"
#include "rand_sampling.h"
#include "heap.h"
#include "sssp.h"
#include "mslc.h"

/* ------------------------------------------------------------ *
 * interface
 * ------------------------------------------------------------ */
static struct profile_t compute_kernel(graph *G_addr,
				       struct blocked_vertices_t *roots,
				       struct cent_param_t *params,
				       struct cent_metric_t *metrics);

struct profile_t compute_dist_kernel(graph *G, struct cent_param_t *params, struct cent_metric_t *metrics) {
  double init_time, comp_time;

  /* source sampling */
  struct blocked_vertices_t roots;
  PROFILED( init_time, roots = make_blocked_vertices(G, params->mslc_beta) );
  printf("finished blocked-source sampling (%.2f msec.)\n", init_time);
  printf("\n");
  
  /* thread-parallel computation */
  struct profile_t prof;
  PROFILED( comp_time, prof = compute_kernel(G, &roots, params, metrics) );
  printf("finished computing distance table (%.2f msec.)\n", comp_time);
  printf("\n");
  
  free(roots.start);
  free(roots.sources);
  
  return prof;
}




/* ------------------------------------------------------------ *
 * compute_kernel
 * ------------------------------------------------------------ */
struct dist_tmp_t {
  node_t *ND;
  heap_t *HQ;
  SPINT *ds;
  SPINT *hops;
  succ_t *succ_list;
  SPINT *succ_count;
  SPINT *queue;
  double *sigma;
};

static struct cent_result_t compute_vertex_iter(struct cent_param_t *params,
						graph *G, int64_t *roots, int64_t nv,
						struct dist_tmp_t *tmp,
						struct cent_metric_t *metrics,
						struct cent_result_t *results);
void update_maxhops_node(struct cent_result_t r);
void print_titlebar(long n, long m, long k);
void print_vertex_iter(graph *G, struct cent_param_t *params,
		       struct cent_result_t result, int64_t itenumber);
static struct dist_tmp_t *alloc_cent_temps(graph *G, struct cent_param_t *params);
static void free_cent_temps(struct cent_param_t *params, struct dist_tmp_t *tmp);

/* NUMA node local variables */
static unsigned char *pool[MAX_NODES] = { NULL };

/* thread local variables */
static struct nodehop_t { int64_t hops, index; } tls_nodehop[MAX_THREADS] = {{ 0, -1 }};

static struct profile_t compute_kernel(graph *G_addr,
				       struct blocked_vertices_t *roots,
				       struct cent_param_t *params,
				       struct cent_metric_t *metrics) {
  int64_t nblocks = (roots->nblocks*params->num_srcs / roots->nv) + 1;
  if (nblocks > roots->nblocks) nblocks = roots->nblocks;
  printf("nblocks: %" PRId64 " of %" PRId64 " (block_size: %" PRId64 ")\n",
	 nblocks, roots->nblocks, roots->block_size);
  printf("accuracy: %f\n", 1.0*nblocks/roots->nblocks);
  
  struct cent_result_t *results = NULL;
  results = calloc(G_addr->n, sizeof(struct cent_result_t));
  assert(results);

  set_timer( 1.0, params->time_limit );
  set_culling( 20 );

  _Pragma("omp parallel") {
    ULIBC_bind_thread_explicit( ULIBC_get_thread_num() );
    struct numainfo_t loc = ULIBC_get_numainfo( ULIBC_get_thread_num() );
    if ( loc.core == 0 ) {
      pool[loc.node] = (loc.node == 0) ?
	(unsigned char *)G_addr : localcopy_graph(G_addr, loc.node);
      printf("NUMA Node %02d warmed up.\n", loc.node);
    }
  }
  printf("\n");
  
  int64_t shared_index = 0;
  
  _Pragma("omp parallel") {
    ULIBC_bind_thread_explicit( ULIBC_get_thread_num() );
    struct numainfo_t loc = ULIBC_get_numainfo( ULIBC_get_thread_num() );
    
    /* allocate thread local variables */
    graph *G = (graph *)pool[loc.node];
    struct dist_tmp_t *tmp = alloc_cent_temps(G, params);

    /* main loop */
    while (1) {
      int64_t start, length;
      _Pragma("omp critical") {
	start = shared_index;
	length = (nblocks - start > 512) ? 8 : 1;
	shared_index += length;
      }
      if (start >= nblocks) break;
    
      for (int64_t i = start; i < start+length; ++i) {
	int64_t *curr_roots = &roots->sources[ roots->start[i] ];
	const int64_t nv = roots->start[i+1] - roots->start[i];
	struct cent_result_t r;
	r = compute_vertex_iter(params, G, curr_roots, nv, tmp, metrics,
				&results[roots->start[i]]);
	update_maxhops_node(r);
	print_vertex_iter(G, params, r, roots->start[i]);
      }
      
      if ( timelimit() < 0 ) break;
    }
    _Pragma("omp barrier");

    if (params->verbose)
      printf("%d-th thread finished\n", loc.id);
    _Pragma("omp barrier");

    /* free thread local variables */
    free_cent_temps(params, tmp);
  }
  printf("\n");

  metrics->diam_hops = 0;
  metrics->diam_id = -1;
  for (int id = 0; id < ULIBC_get_num_threads(); ++id) {
    if (metrics->diam_hops < tls_nodehop[id].hops) {
      metrics->diam_hops = tls_nodehop[id].hops;
      metrics->diam_id = tls_nodehop[id].index;
    }
  }

  /* free NUMA local graphs */
  for (int i = 0; i < ULIBC_get_online_nodes(); ++i) {
    NUMA_free(pool[i]);
  }

  struct cent_result_t total = {
    .s = -1,
    .trav_nodes = 0,
    .trav_edges = 0,
    .max_hops   = 0,
    .sssp_ms    = 0.0,
    .update_ms  = 0.0
  };

  for (int64_t i = 0; i < G_addr->n; ++i) {
    if (total.max_hops < results[i].max_hops) {
      total.s = results[i].s;
      total.max_hops = results[i].max_hops;
    }
    total.trav_nodes += results[i].trav_nodes;
    total.trav_edges += results[i].trav_edges;
    total.sssp_ms    += results[i].sssp_ms;
    total.update_ms  += results[i].update_ms;
  }

  struct profile_t prof = {
    .iterations = roots->nv,
    .total = total,
    .results = results,
  };
  return prof;
}


static struct dist_tmp_t *alloc_cent_temps(graph *G, struct cent_param_t *params) {
  struct dist_tmp_t *tmp = (struct dist_tmp_t *)malloc(sizeof(struct dist_tmp_t));
  assert( tmp );
  
  tmp->ND = (node_t *)malloc(sizeof(node_t) * G->n);
  tmp->HQ = (heap_t *)malloc(sizeof(heap_t) * G->n);
  assert( tmp->ND && tmp->HQ );
  
  if (params->mslc_beta == 1) {
    if (params->hop_bound) {
      tmp->ds   = (SPINT *)malloc(sizeof(SPINT) * G->n);
      tmp->hops = (SPINT *)tmp->ds;
    } else {
      tmp->ds   = (SPINT *)tmp->ND; /* reuse */
      tmp->hops = (SPINT *)tmp->ND;
    }
    assert( tmp->ds && tmp->hops );
  } else {
    tmp->ds = (SPINT *)malloc(sizeof(SPINT) * params->mslc_beta * G->n);
    assert( tmp->ds );
  }
  
  tmp->queue = (SPINT *)malloc(sizeof(SPINT) * (G->n+1));
  assert( tmp->queue );
  
  if (params->mslc_beta == 1) {
    tmp->succ_list  = (succ_t *)malloc(sizeof(succ_t) * G->m); /* m */
    tmp->succ_count = (SPINT  *)malloc(sizeof(SPINT)  * G->n);
    tmp->sigma      = (double *)malloc(sizeof(double) * G->n);
    assert( tmp->succ_list && tmp->succ_count && tmp->sigma );
  }
  return tmp;
}


static void free_cent_temps(struct cent_param_t *params, struct dist_tmp_t *tmp) {
  free( tmp->ND );
  free( tmp->HQ );
  if (params->mslc_beta == 1) {
    if (params->hop_bound) {
      free( tmp->ds );
    }
  } else {
    free( tmp->ds );
  }
  free( tmp->queue );
  if (params->mslc_beta == 1) {
    free( tmp->succ_list );
    free( tmp->succ_count );
    free( tmp->sigma );
  }
  free( tmp );
}





/* ------------------------------------------------------------ *
 * compute_vertex_iter
 * ------------------------------------------------------------ */
static struct cent_result_t compute_vertex_iter(struct cent_param_t *params,
						graph *G, int64_t *roots, int64_t nv,
						struct dist_tmp_t *tmp,
						struct cent_metric_t *metrics,
						struct cent_result_t *results) {
  double *CC, *GC;
  CC = calloc(nv, sizeof(int64_t));
  GC = calloc(nv, sizeof(int64_t));
  
  double t;
  
  /* shortest-paths */
  if ( metrics->CC || metrics->EC || metrics->GC || metrics->TC ) {
    if (params->mslc_beta > 1) {
      /* mslc */
      memset((void *)tmp->ND, 0xff, G->n * sizeof(node_t));
      memset((void *)tmp->ds, 0xff, G->n * params->mslc_beta * sizeof(SPINT));
      
      result_t r;
      if (params->weighted) {
	PROFILED( t, r = MSSP_mslc_weighted(params->mslc_beta,
					    G->forward, G->head, G->length,
					    nv, roots, tmp->ND, tmp->ds, tmp->HQ) );
      } else {
	PROFILED( t, r = MSSP_mslc(params->mslc_beta, G->forward, G->head, G->length,
				   nv, roots, tmp->ND, tmp->ds, tmp->HQ) );
      }
      const int64_t beta = params->mslc_beta;
      for (int64_t v = 0; v < G->n; ++v) {
	for (int64_t j = 0; j < nv; ++j) {
	  const int64_t v_j = v * beta + j;
	  const int64_t d = tmp->ds[v_j];
	  if (d >= 0) {
	    CC[j] += d;
	    GC[j] = d > GC[j] ? d : GC[j];
	  }
	}
      }
      for (int64_t j = 0; j < nv; ++j) {
	results[j].s          = roots[j];
	results[j].trav_nodes = r.trav_nodes / nv;
	results[j].trav_edges = r.trav_edges / nv;
	results[j].max_hops   = GC[j];
	results[j].sssp_ms    = t / nv;
	/* results[j].update_ms; */
      }
    } else {
      /* shortest-paths */
      for (int64_t j = 0; j < nv; ++j) {
	const int64_t s = roots[j];
	result_t r;
	if (params->weighted) {
	  memset((void *)tmp->ND, 0xff, G->n * sizeof(node_t));
	  tmp->succ_count[s] = G->forward[s];
	  PROFILED( t, r = MP_SSSP_dijkstra(G->forward, G->head, G->length, s,
					    tmp->ND, tmp->HQ,
					    tmp->succ_list, tmp->succ_count,
					    tmp->queue, tmp->sigma) );
	} else {
	  memset((void *)tmp->ds, 0xff, G->n * sizeof(SPINT));
	  PROFILED( t, r = MP_BFS(G->forward, G->head, s,
				  tmp->ds, tmp->succ_list, tmp->succ_count,
				  tmp->queue, tmp->sigma) );
	}
	CC[j] = r.CC;
	GC[j] = r.GC;
	results[j].s          = roots[j];
	results[j].trav_nodes = r.trav_nodes;
	results[j].trav_edges = r.trav_edges;
	results[j].max_hops   = GC[j];
	results[j].sssp_ms    = t;
	/* results[j].update_ms; */
      }
    }
  }
 
  /* update centrality metrics */
  for (int64_t j = 0; j < nv; ++j) {
    double t1 = get_msecs(), t2;
    const int64_t s = roots[j];

    /* update DC */
    if ( metrics->DC ) {
      const int64_t deg = G->forward[s+1] - G->forward[s];
      metrics->DC[s] = (double)deg * DC_BASE(G->n);
    }

    /* update CC, GC */
    if ( metrics->CC && CC[j] != 0.0 ) {
      metrics->CC[s] = CC_BASE(G->n) / CC[j];
      /* metrics->CC[s] = CC[j]; */
    }
    if ( metrics->GC && GC[j] != 0.0 ) {
      metrics->GC[s] = GC_BASE(G->n) / GC[j];
      /* metrics->GC[s] = GC[j]; */
    }

    /* update EC, TC */
#if 0
    if ( G->X && G->Y && ( metrics->EC || metrics->TC ) ) {
      int64_t dist_v;
      double TC_s = 0.0, EC_s = 0.0;
      double eucl_s_v, eucl_s = 0.0;
      double X_s = G->X[s], Y_s = G->Y[s];
      for (int64_t v = 0; v < G->n; ++v) {
	eucl_s_v = fsqrt( (X_s - G->X[v]) * (X_s - G->X[v]) + 
			  (Y_s - G->Y[v]) * (Y_s - G->Y[v]) );
	if ( eucl_s_v != 0.0 ) {
	  eucl_s += 1.0 / eucl_s_v;
	}
	if (params->weighted) {
	  dist_v = tmp->ND[v].key;
	} else {
	  dist_v = tmp->ds[v];
	}
	if ( dist_v > 0 ) {
	  EC_s += 1.0 / dist_v;
	  TC_s += eucl_s_v / dist_v;
	}
      }
      if (metrics->EC && eucl_s != 0.0) {
	metrics->EC[s] = EC_BASE(G->n) * EC_s / eucl_s;
      }
      if (metrics->TC) {
	metrics->TC[s] = TC_s * TC_BASE(G->n);
      }
    }
#endif
    t2 = get_msecs();
    results[j].update_ms = t2 - t1;
  }

  free(CC);
  free(GC);

  struct cent_result_t R;
  memset(&R, 0x00, sizeof(struct cent_result_t));
  for (int64_t j = 0; j < nv; ++j) {
    R.trav_nodes += results[j].trav_nodes;
    R.trav_edges += results[j].trav_edges;
    if (R.max_hops < results[j].max_hops) {
      R.s = results[j].s;
      R.max_hops = results[j].max_hops;
    }
    R.sssp_ms += results[j].sssp_ms;
    R.update_ms += results[j].update_ms;
  }
  
  return R;
}
