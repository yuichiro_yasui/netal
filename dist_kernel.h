#ifndef DISTANCE_H
#define DISTANCE_H

#include "cent_kernel.h"

#if defined (__cplusplus)
extern "C" {
#endif
  struct profile_t compute_dist_kernel(graph *G,
				       struct cent_param_t *params,
				       struct cent_metric_t *metrics);
  void dump_dist_metrics(char *filename, char *desc, char *kernel,
			 graph *G,
			 struct cent_param_t *params,
			 struct cent_metric_t *metrics);
#if defined (__cplusplus)
}
#endif

#endif /* DISTANCE_H */
