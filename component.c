#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>

#include "ulibc.h"
#include "defs.h"
#include "utls.h"
#include "graph.h"

/* kernel */
#include "component.h"

/* ------------------------------------------------------------ *
 * make_colors()
 * ------------------------------------------------------------ */
struct profile_t make_colors(graph *G) {
  I64_t i, j, k, nchanged;
  I64_t n = G->n, m = G->m;
  SPINT *forward = G->forward, *head = G->head;
  I64_t *marks = (I64_t *)malloc(n * sizeof(I64_t));
  I64_t ites = 0, trav = 0;
  
  for (i = 0; i < n; ++i) {
    marks[i] = i;
  }
  do {
    ites++;
    nchanged = 0;
    /* forward-search */
    trav += m;
    for (i = 0; i < n; ++i) {
      for (k = forward[i]; k < forward[i+1]; ++k) {
	j = head[k];
	if (marks[i] < marks[j]) {
	  /* update root-mark */
	  marks[marks[j]] = marks[i];
	  ++nchanged;
	}
      }
    }
    /* update 'not' root-mark */
    for (i = 0; i < n; ++i) {
      if (marks[i] != marks[marks[i]]) {
	marks[i] = marks[marks[i]];
      }
    }
  } while (nchanged);
  
  if (G->color) free(G->color);
  G->color = marks;
    
  struct profile_t p;
  p.iterations = ites;
  memset(&p.total, 0x00, sizeof(struct cent_result_t));
  p.results    = NULL;
  return p;
}



/* ------------------------------------------------------------ *
 * construct_comp_graph()
 * ------------------------------------------------------------ */
int compcmp(const void *a, const void *b) {
  struct comp_t _a = *(struct comp_t *)a;
  struct comp_t _b = *(struct comp_t *)b;
  if (_a.elements < _b.elements) {
    return 1;
  } else if (_a.elements > _b.elements) {
    return -1;
  } else {
    return (_b.root < _a.root) ? 1 : -1;
  }
}

struct comp_graph_t *construct_comp_graph(graph *G, int top) {
  struct comp_graph_t *cg = NULL;
  assert( cg = (struct comp_graph_t *)malloc(sizeof(struct comp_graph_t)) );
  cg->n = G->n;
  cg->m = G->m;
  assert( cg->compid = (I64_t *)malloc(sizeof(I64_t) * cg->n) );
  
  I64_t i, j, v, w, l, num_comps = 0, *root = NULL, *elements = NULL;
  struct edge_t *E = NULL;

  /* counting components */
  assert( root = (I64_t *)malloc(G->n * sizeof(I64_t)) );
  assert( elements = (I64_t *)malloc(G->n * sizeof(I64_t)) );
  for (i = 0; i < G->n; ++i) {
    root[i] = elements[i] = -1;
  }
  for (i = 0; i < G->n; ++i) {
    elements[G->color[i]]++;
    if (G->color[i] == i) {
      root[num_comps++] = i;
    }
  }
  /* for (i = 0; i < num_comps; ++i) { */
  /*   printf("roor is %lld, #elements is %lld\n", root[i], elements[root[i]]); */
  /* } */
  cg->num_comps = num_comps;
  assert( cg->subg = (graph **)calloc(cg->num_comps, sizeof(graph *)) );
  
  /* sorting components */
  assert( cg->comps = (struct comp_t *)malloc((num_comps) * sizeof(struct comp_t)) );
  for (i = 0; i < num_comps; ++i) {
    cg->comps[i].root  = root[i];
    cg->comps[i].elements = elements[root[i]];
  }
  free(root);
  free(elements);
  qsort(cg->comps, num_comps, sizeof(struct comp_t), compcmp);
  
  /* constuct component graph */
  printf("#components is %lld\n", num_comps);
  
  assert( E = (struct edge_t *)malloc((cg->m+1) * sizeof(struct edge_t)) );
  for (i = 0; i < num_comps; ++i) {
    if (i < 10) {
      printf("[%02lld] root is %6lld, #elements is %6lld\n", 
	     i+1, cg->comps[i].root+1, cg->comps[i].elements);
    }
    
    /* extracting component edges */
    I64_t col = cg->comps[i].root;
    I64_t lm = 0;
    for (v = 0; v < G->n; ++v) {
      if (G->color[v] == col) {
	cg->compid[v] = i;
	for (j = G->forward[v]; j < G->forward[v+1]; ++j) {
	  w = G->head[j];
	  l = G->length[j];
	  if (G->color[w] == col) {
	    E[lm].v = v;
	    E[lm].w = w;
	    E[lm].l = l;
	    lm++;
	  }
	}
      }
    }
    E[lm].v = -1;
    E[lm].w = -1;
    E[lm].l = -1;
    
    /* constructing sub-graph */
    cg->subg[i] = construct_graph(E, G->X, G->Y);
    if (i + 1 == top) break;
  }
  free(E);
  
  return cg;
}


void free_comp_graph(struct comp_graph_t *cg) {
  int i;
  if (cg) {
    free(cg->compid);
    free(cg->comps);
    for (i = 0; i < cg->num_comps; ++i) {
      free_graph(cg->subg[i]);
    }
    free(cg->subg);
    free(cg);
  }
}



/* ------------------------------------------------------------ *
 * dump_colors()
 * ------------------------------------------------------------ */
void dump_colors(char *filename, char *desc, struct comp_graph_t *cg) {
  if (!filename) {
    return;
  }
  
  FILE *fp = fopen(filename, "w");
  fprintf(fp, "c file name: %s\n", filename);
  if (desc) {
    fprintf(fp, "c instance name: %s\n", desc);
  }
  
  time_t now = time(NULL);
  fprintf(fp, "c created: %s", ctime(&now));
  fprintf(fp, "c\n");
  fprintf(fp, "p sp %lld %lld\n", (I64_t)cg->n, (I64_t)cg->m);
  fprintf(fp, "c\n");
  fprintf(fp, "l component ID\n");
  fprintf(fp, "l root ID\n");
  fprintf(fp, "c\n");
  fprintf(fp, "c\n");
  
  I64_t i, j;
  for (j = 0; j < cg->num_comps; ++j) {
    fprintf(fp, "c %02lld-th components : root is %lld, #elements is %lld\n", 
	    j+1, cg->comps[j].root+1, cg->comps[j].elements);
  }
  fprintf(fp, "c\n");
  fprintf(fp, "c\n");
  
  for (i = 0; i < cg->n; ++i) {
    j = cg->compid[i];
    fprintf(fp, "d %lld %lld %lld\n", i+1, j+1, cg->comps[j].root+1);
  }
  
  fclose(fp);
}
