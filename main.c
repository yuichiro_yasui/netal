#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include <assert.h>
#include <limits.h>
#include <ulibc.h>

/* utilities */
#include "defs.h"
#include "options.h"
#include "utls.h"
#include "graph.h"
#include "degree.h"
#include "stat.h"
#include "log.h"

/* kernels */
#include "cent_kernel.h"
#include "dist_kernel.h"
#include "component.h"

#define TO_S(x) ((x) ? #x ", " : "")

int main(int argc, char **argv) {
  ULIBC_init();
  
  double t1, t2;
  
  struct netal_option_t op = get_netal_options(argc, argv);
  setbuf(stdout, NULL);
  if ( !op.graph_file ) {
    printf("[error] graph file is not specified\n");
    return 1;
  }
  /* if ( !op.kernel_id ) { */
  /*   printf("[error] kernel is not specified\n"); */
  /*   return 1; */
  /* } */
  printf("graph file     is %s %s(%d)\n", op.graph_file, op.graph_desc, op.graph_type);
  printf("query file     is %s\n", op.query_file  ? op.query_file  : "not specified");
  printf("dump file      is %s\n", op.dump_file   ? op.dump_file   : "not specified");
  printf("kernel         is %s\n", op.kernel_name ? op.kernel_name : "not specified");
  printf("\n");
  
  
  printf("parsing edge file ...\n");
  t1 = get_msecs();
  struct edge_t *edges = parse_edges(op.graph_file, op.graph_type);
  if ( getenv("DUMPUNIQTABLE") ) {
    I64_t n, m, *table = NULL;
    uniq_edgelist(edges, &n, &m, &table);
    dump_convert_table(getenv("DUMPUNIQTABLE"), n, m, table);
    free(table);
  }
  t2 = get_msecs();
  if ( !edges ) {
    printf("[error] can't parse edges\n");
    return 1;
  } else {
    printf("done. (%.2f msec.)\n", t2 - t1);
  }
  if ( !dump_edges(getenv("DUMPEDGES"), getenvi("DUMPETYPE", 0), edges) ) {
    printf("dump dimacs edges '%s'\n", getenv("DUMPEDGES"));
  }
  
  
  double *X=NULL, *Y=NULL;
  if ( getenv("DIMACS_CO") ) {
    long n;
    printf("parsing coordinates file ...\n");
    t1 = get_msecs();
    parse_coords(getenv("DIMACS_CO"), &n, &X, &Y);
    t2 = get_msecs();
    if ( !X || !Y ) {
      printf("[error] can't parse coordinates\n");
      return 1;
    } else {
      printf("done. (%.2f msec.)\n", t2 - t1);
    }
  }
  
  
  printf("constructing graph file ...\n");
  t1 = get_msecs();
  if (getenvi("UNDIRECTED", 0)) {
    printf("convert undirected graph ... \n");
    edges = undirected_filter(edges);
    printf("done.\n");
  }
  graph *g = construct_graph(edges, X, Y);
  free(edges);
  free(X);
  free(Y);
  t2 = get_msecs();
  if ( !g ) {
    printf("[error] can't construct graph\n");
    return 1;
  } else {
    printf("done. (%.2f msec.)\n", t2 - t1);
  }
  printf("graph          is n=%" PRId64 ", m=%" PRId64 ", edgefactor=m/n=%.2f\n",
         g->n, g->m, 1.0*g->m/g->n);
  printf("\n");

  
  /* ---------- degree distributions ---------- */
  t1 = get_msecs();
  degdist fd = degree_distributions(g->n, g->forward);
  degdist bd = degree_distributions(g->n, g->backward);
  t2 = get_msecs();
  printf("calculate out/in-degree distributions ...\n");
  printf("[out] {max,avg,E[X^2],var,stddev} is %lld %g %g %g %g\n",
         (I64_t)fd.maxdeg, fd.avg, fd.avg_sq, fd.var, fd.std_dev);
  printf("[in ] {max,avg,E[X^2],var,stddev} is %lld %g %g %g %g\n",
         (I64_t)bd.maxdeg, bd.avg, bd.avg_sq, bd.var, bd.std_dev);
  printf("done. (%.2f msec.)\n", t2-t1);
  printf("\n");
  

  double kernel_time = 0.0;
  struct profile_t p;
  memset(&p, 0x00, sizeof(struct profile_t));

  /* ---------- components ---------- */
  if ( op.kernel_id == KERNEL_COMP ) {
    t1 = get_msecs();
    printf("finding components ...\n");
    p = make_colors(g);
    struct comp_graph_t *cg = construct_comp_graph(g, 0);
    t2 = get_msecs();
    kernel_time = t2 - t1;
    if ( op.kernel_flag &
         symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_COMP].sub, "c") ) {
      printf("dumping vertex color\n");
      dump_colors(op.dump_file, ubasename(op.graph_file), cg);
    }
    if ( op.kernel_flag &
         symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_COMP].sub, "s") ||
         op.kernel_flag &
         symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_COMP].sub, "m") ) {
      int maxCC = op.kernel_flag &
        symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_COMP].sub, "m");
      I64_t i;
      int log10_n = intlog(10, cg->n), log10_m = intlog(10, cg->m);
      char *name=NULL, dumpname[PATH_MAX];
      name = extract_suffix(ubasename(op.graph_file), ".gr");
      for (i = 0; i < cg->num_comps; ++i) {
        sprintf(dumpname, "%s.%lld.gr", name, i+1);
        printf("dumping [%lld]-th sub-graph"
               " (uniq(n)=%*" PRId64 ", m=%*" PRId64 ", max(V)=%*" PRId64 ") -> '%s'\n",
               i+1,
               log10_n, uniq_nodes(cg->subg[i]),
               log10_m, cg->subg[i]->m,
               log10_n, cg->subg[i]->n,
               dumpname);
        dump_dimacs_graph(dumpname, cg->subg[i]);
        if (maxCC) break;
      }
      free(name);
    }
    free_comp_graph(cg);
  }
  /* ---------- components ---------- */
  
  /* ---------- centrality ---------- */
  if ( op.kernel_id == KERNEL_CENT ) {
    int is_maxcc_cent = op.kernel_flag & 
      symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_CENT].sub, "m");
    graph *G_addr = g;
    struct comp_graph_t *cg = NULL;
    
    if ( is_maxcc_cent ) {
      printf("finding components ...\n");
      make_colors(g);
      cg = construct_comp_graph(g, 1);
      G_addr = cg->subg[0];     /* maximum connected-component */
    }
    
    struct cent_metric_t CM;
    memset(&CM, 0x00, sizeof(struct cent_metric_t));

    if ( op.kernel_flag & symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_CENT].sub, "C") )
      CM.CC  = (double *)calloc(G_addr->n, sizeof(double)), assert( CM.CC  );
    if ( op.kernel_flag & symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_CENT].sub, "D") )
      CM.DC  = (double *)calloc(G_addr->n, sizeof(double)), assert( CM.DC  );
    if ( op.kernel_flag & symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_CENT].sub, "E") )
      CM.EC  = (double *)calloc(G_addr->n, sizeof(double)), assert( CM.EC  );
    if ( op.kernel_flag & symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_CENT].sub, "G") )
      CM.GC  = (double *)calloc(G_addr->n, sizeof(double)), assert( CM.GC  );
    if ( op.kernel_flag & symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_CENT].sub, "T") )
      CM.TC  = (double *)calloc(G_addr->n, sizeof(double)), assert( CM.TC  );
    if ( op.kernel_flag & symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_CENT].sub, "S") ) { 
      CM.SC  = (double *)calloc(G_addr->n, sizeof(double)), assert( CM.SC  );
      CM.eSC = (double *)calloc(G_addr->m, sizeof(double)), assert( CM.eSC );
    }
    if ( op.kernel_flag & symbol_to_flag(usages[USG_KERNEL].sub[KERNEL_CENT].sub, "B") ) {
      CM.BC  = (double *)calloc(G_addr->n, sizeof(double)), assert( CM.BC  );
      CM.eBC = (double *)calloc(G_addr->m, sizeof(double)), assert( CM.eBC );
    }
    
    if ( CM.EC || CM.TC ) {
      if ( !g->X || !g->Y ) {
        printf("[error] not found XY coordinates.\n");
        printf("[error] Efficiency and Straightness are raquired coordinates.\n");
        printf("please set path of coordinates file (*.co) to "
               "environment variable 'DIMACS_CO'.\n");
        exit(1);
      }
    }
    
    printf("compute %s%s%s%s%s%s%s%s%s\n",
           TO_S(CM.CC), TO_S(CM.DC), TO_S(CM.EC), TO_S(CM.GC), TO_S(CM.TC),
           TO_S(CM.SC), TO_S(CM.eSC), TO_S(CM.BC), TO_S(CM.eBC));

    t1 = get_msecs();
    if ( getenvi("FALL_TOLERANCE", 0) ) {
      I64_t i, maxites = 10, num_srcs = G_addr->n * op.accuracy;
      I64_t chunk = num_srcs / maxites, fixed = 0;
      struct cent_result_t total;
      memset(&total, 0x00, sizeof(struct cent_result_t));
      struct cent_param_t CP = {
        .startid   = 0,
        .num_srcs  = G_addr->n * op.accuracy,
        .weighted  = op.weighted,
        .hop_bound = op.hop_bound,
        .mslc_beta = op.mslc_beta,
        .verbose   = op.verbose,
        .sequence  = op.sequence,
        .time_limit = getenvf("TIMELIMIT", 0.0)
      };
      printf("chunk, #srcs is %lld %lld\n", chunk, num_srcs);
      for (i = 0; i < maxites; ++i) {
        CP.startid = fixed;
        CP.num_srcs = (i == maxites-1) ? (num_srcs - fixed) : chunk;
        printf("\n[%02lld] start, end is %" PRId64 " %" PRId64 "\n",
               i+1, CP.startid, CP.startid + CP.num_srcs);
        p = compute_cent_kernel(G_addr, &CP, &CM);
        total.s           = (total.max_hops < p.total.max_hops) ? p.total.s : total.s;
        total.max_hops    = (total.max_hops < p.total.max_hops) ? p.total.max_hops : total.max_hops;
        total.trav_nodes += p.total.trav_nodes;
        total.trav_edges += p.total.trav_edges;
        total.sssp_ms    += p.total.sssp_ms;
        total.update_ms  += p.total.update_ms;
        /* printf("%lld %lld\n", p.total.max_hops, p.total.s); */
        fixed += chunk;
      
        /* dump */
        CP.num_srcs = fixed;
        CM.diam_hops = total.max_hops;
        CM.diam_id = total.s;
        dump_cent_metrics(op.dump_file, ubasename(op.graph_file), op.kernel_name, G_addr, &CP, &CM);
      }
      p.total = total;
    } else {
      struct cent_param_t CP = {
        .num_srcs  = G_addr->n * op.accuracy,
        .startid   = 0,
        .weighted  = op.weighted,
        .hop_bound = op.hop_bound,
        .mslc_beta = op.mslc_beta,
        .verbose   = op.verbose,
        .sequence  = op.sequence,
        .time_limit = getenvf("TIMELIMIT", 0.0)
      };
      p = compute_cent_kernel(G_addr, &CP, &CM);
      dump_cent_metrics(op.dump_file, ubasename(op.graph_file), op.kernel_name, G_addr, &CP, &CM);
    }
    t2 = get_msecs();
    kernel_time = t2 - t1;
  
    /* dump centrality indices */
    if (CM.BC && getenv("DUMPGRAPHCT")) {
      FILE *fp = fopen(getenv("DUMPGRAPHCT"), "w");
      SPINT v;
      for (v = 0; v < G_addr->n; ++v) {
        fprintf(fp, "%23.15e\n", CM.BC[v]);
      }
      fclose(fp);
    }
    free(CM.CC);
    free(CM.DC);
    free(CM.EC);
    free(CM.GC);
    free(CM.TC);
    free(CM.SC);
    free(CM.eSC);
    free(CM.BC);
    free(CM.eBC);
    
    if (is_maxcc_cent) {
      free_comp_graph(cg);
    }
  }
  /* ---------- centrality ---------- */
 
  
  /* ---------- distance ---------- */
  if ( op.kernel_id == KERNEL_MSSP ) {
    struct cent_metric_t metrics;
    memset(&metrics, 0x00, sizeof(struct cent_metric_t));
    assert( metrics.CC = (double *)calloc(g->n, sizeof(double)) );
    assert( metrics.DC = (double *)calloc(g->n, sizeof(double)) );
    assert( metrics.GC = (double *)calloc(g->n, sizeof(double)) );
    printf("compute %s%s%s\n", TO_S(metrics.CC), TO_S(metrics.DC), TO_S(metrics.GC));

    t1 = get_msecs();
    struct cent_param_t params = {
      .num_srcs  = g->n * op.accuracy,
      .startid   = 0,
      .weighted  = op.weighted,
      .hop_bound = 0,           /* always ignored */
      .mslc_beta = op.mslc_beta,
      .verbose   = op.verbose,
      .sequence  = op.sequence,
      .time_limit = getenvf("TIMELIMIT", 0.0)
    };
    p = compute_dist_kernel(g, &params, &metrics);
    dump_cent_metrics(op.dump_file, ubasename(op.graph_file), op.kernel_name,
                      g, &params, &metrics);
    t2 = get_msecs();
    kernel_time = t2 - t1;
    
    /* dump centrality indices */
    free(metrics.CC);
    free(metrics.DC);
    free(metrics.GC);
  }
  /* ---------- distance ---------- */
  
  printf("\n");
  logprintf("%s", desc);
  logprintf("Machine: %d sockets (%d cores), %.2f GB RAM\n",
            ULIBC_get_online_nodes(),
            ULIBC_get_online_procs(),
            (double)ULIBC_total_memory_size()/(1ULL<<30));
  logprintf("----------------------------------------------------------------------\n");
  logprintf("graph file     is %s %s(%d)\n",
            op.graph_file, op.graph_desc, op.graph_type);
  logprintf("dump file      is %s\n",
            op.dump_file ? op.dump_file : "not specified" );
  logprintf("kernel         is %s\n", op.kernel_name);
  logprintf("graph          is n=%lld(log2{n}=%.2f), m=%lld(log2{m}=%.2f), m/n=%.2f\n",
            (I64_t)g->n, log2(g->n), (I64_t)g->m, log2(g->m), 1.0*g->m/g->n);
  if (p.total.max_hops > 0)
    logprintf("diameter       is %lld (s=%lld)\n", p.total.max_hops, p.total.s+1);
  logprintf("out-degree     is {max,avg,E[X^2],var,stddev} is %lld %g %g %g %g\n",
            (I64_t)fd.maxdeg, fd.avg, fd.avg_sq, fd.var, fd.std_dev);
  logprintf("in-degree      is {max,avg,E[X^2],var,stddev} is %lld %g %g %g %g\n",
            (I64_t)bd.maxdeg, bd.avg, bd.avg_sq, bd.var, bd.std_dev);
  logprintf("traversal      is %lld nodes and %lld edges\n",
            p.total.trav_nodes, p.total.trav_edges);
  const int hh = kernel_time * 1e-3 / 3600;
  const int mm = (kernel_time * 1e-3 - 3600 * hh) / 60;
  const double ss = kernel_time * 1e-3 - 3600 * hh - 60 * mm;
  logprintf("cpu time       is %.3f seconds (%02d:%02d:%05.2f) (#Threads=%d)\n",
            kernel_time*1e-3, hh, mm, ss, ULIBC_get_num_threads());
  logprintf("profile        is spaths: %.3fms (%.3f%%), update: %.3fms (%.3f%%)\n",
            p.total.sssp_ms/ULIBC_get_num_threads(),
            p.total.sssp_ms/ULIBC_get_num_threads()/kernel_time*1e2,
            p.total.update_ms/ULIBC_get_num_threads(),
            p.total.update_ms/ULIBC_get_num_threads()/kernel_time*1e2);
  
  if ( p.results ) {
    /* output_results(p.iterations, p.sssp_times, p.trav_edges); */
    free(p.results);
  }
  
  free_graph(g);
  finalize_netal_options(op);
  return 0;
}


/*
 * Local variables:
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * End:
 */
